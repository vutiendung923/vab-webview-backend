package com.xdev.vab.booking.sys.service.dto.WebPage;

import com.xdev.vab.booking.sys.model.PageConfig;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageConfigDetailDto {
    private Integer id;
    private String namePage;
    private String url;
    private String title;
    private String content;
    private Integer pageId;
    private String path;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Integer id;
        private String namePage;
        private String url;
        private String title;
        private String content;
        private Integer pageId;
        private String path;

        public Builder pageConfigDetail(PageConfig pageConfig) {
            this.id = pageConfig.getId();
            this.namePage = pageConfig.getNamePage();
            this.url = pageConfig.getUrl();
            this.title = pageConfig.getTitle();
            this.content = pageConfig.getContent();
            this.pageId = pageConfig.getWebPage().getId();
            this.path = pageConfig.getWebPage().getUrl();
            return this;
        }

        public PageConfigDetailDto build() {
            return new PageConfigDetailDto(id, namePage, url, title, content, pageId, path);
        }
    }
}
