package com.xdev.vab.booking.sys.model;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class BaseEntity {
    protected Date createTime;
    protected String createBy;
    protected Date updateTime;
    protected String updateBy;
}
