package com.xdev.vab.booking.sys.service.dto.artist;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ArtistDescriptionDto {
    private Integer id;
    private String artisticName;
    private String descriptionShort;
}
