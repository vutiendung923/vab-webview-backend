package com.xdev.vab.booking.sys.dao.impl;

import com.xdev.vab.booking.common.PageData;
import com.xdev.vab.booking.sys.dao.PostDao;
import com.xdev.vab.booking.sys.dao.mapper.PostCategoryMapper;
import com.xdev.vab.booking.sys.dao.mapper.PostMapper;
import com.xdev.vab.booking.sys.model.Post;
import com.xdev.vab.booking.sys.model.PostCategory;
import com.xdev.vab.booking.sys.service.impl.StoreProcedureCaller;
import oracle.jdbc.OracleTypes;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

@Repository
public class PostDaoImpl implements PostDao {
    private final DataSource dataSource;

    public PostDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public PageData<Post> getList(String requestId, Integer langId, Integer pageIndex, Integer pageSize, Integer categoryId) {
        StoreProcedureCaller caller = new StoreProcedureCaller(dataSource, requestId, "PKG_POST", "GET_LIST");
        Map<String, Object> result = caller
                .register("P_CODE", OracleTypes.INTEGER)
                .register("P_DETAIL", OracleTypes.VARCHAR)
                .setValue("P_LANG_ID", langId)
                .setValue("P_INDEX", pageIndex)
                .setValue("P_SIZE", pageSize)
                .setValue("P_CATEGORY_ID", categoryId)
                .register("P_BEGIN_INDEX", OracleTypes.INTEGER)
                .register("P_END_INDEX", OracleTypes.INTEGER)
                .register("P_TOTAL_RECORD", OracleTypes.INTEGER)
                .register("P_TOTAL_PAGE", OracleTypes.INTEGER)
                .register("P_LIST_POST", OracleTypes.CURSOR)
                .returningResultSet("P_LIST_POST", new PostMapper())
                .executeOnError();
        PageData<Post> pageData = new PageData<>();
        pageData.setBeginIndex((Integer) result.get("P_BEGIN_INDEX"));
        pageData.setEndIndex((Integer) result.get("P_END_INDEX"));
        pageData.setTotalRecords((Integer) result.get("P_TOTAL_RECORD"));
        pageData.setTotalPages((Integer) result.get("P_TOTAL_PAGE"));
        pageData.setPageIndex(pageIndex);
        pageData.setPageSize(pageSize);
        pageData.setData((List<Post>) result.get("P_LIST_POST"));
        return pageData;
    }

    @Override
    public Post getDetail(String requestId, Integer langId, Integer id) {
        StoreProcedureCaller caller = new StoreProcedureCaller(dataSource, requestId, "PKG_POST", "GET_DETAIL");
        Map<String, Object> result = caller
                .register("P_CODE", OracleTypes.INTEGER)
                .register("P_DETAIL", OracleTypes.VARCHAR)
                .setValue("P_LANG_ID", langId)
                .setValue("P_ID", id)
                .register("P_INFO", OracleTypes.CURSOR)
                .returningResultSet("P_INFO", new PostMapper())
                .executeOnError();

        return ((List<Post>) result.get("P_INFO")).get(0);
    }

    @Override
    public List<PostCategory> getListCategory(String requestId, Integer langId) {
        StoreProcedureCaller caller = new StoreProcedureCaller(dataSource, requestId, "PKG_POST", "GET_LIST_CATEGORY");
        Map<String, Object> result = caller
                .register("P_CODE", OracleTypes.INTEGER)
                .register("P_DETAIL", OracleTypes.VARCHAR)
                .setValue("P_LANG_ID", langId)
                .register("P_CATEGORIES", OracleTypes.CURSOR)
                .returningResultSet("P_CATEGORIES", new PostCategoryMapper())
                .executeOnError();
        return (List<PostCategory>) result.get("P_CATEGORIES");
    }
}
