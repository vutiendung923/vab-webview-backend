package com.xdev.vab.booking.sys.repository;

import com.xdev.vab.booking.sys.model.ArtistProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ArtistProductRepo extends JpaRepository<ArtistProduct, Integer>, JpaSpecificationExecutor<ArtistProduct> {
}
