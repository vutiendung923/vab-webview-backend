package com.xdev.vab.booking.sys.dao.mapper;

import com.xdev.vab.booking.sys.model.PostCategory;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PostCategoryMapper implements RowMapper<PostCategory> {
    @Override
    public PostCategory mapRow(ResultSet resultSet, int i) throws SQLException {
        PostCategory postCategory = new PostCategory();
        postCategory.setId(resultSet.getInt("ID"));
        postCategory.setImage(resultSet.getString("IMAGE"));
        postCategory.setStatus(resultSet.getInt("STATUS"));
        postCategory.setCreateTime(resultSet.getDate("CREATE_TIME"));
        postCategory.setCreateBy(resultSet.getString("CREATE_BY"));
        postCategory.setUpdateBy(resultSet.getString("UPDATE_BY"));
        postCategory.setUpdateTime(resultSet.getDate("UPDATE_TIME"));
        postCategory.setName(resultSet.getString("NAME"));
        postCategory.setTitle(resultSet.getString("TITLE"));
        postCategory.setLangId(resultSet.getInt("LANG_ID"));
        return postCategory;
    }
}
