package com.xdev.vab.booking.sys.service;


import com.xdev.vab.booking.common.Auth;

public interface JwtService {
    String encode(Auth auth, int hourNumber);

    Auth decode(String jwt);

    boolean isExpiredToken(String token);
}
