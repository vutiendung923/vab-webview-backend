package com.xdev.vab.booking.sys.dao.impl;

import com.xdev.vab.booking.common.Auth;
import com.xdev.vab.booking.sys.dao.AccountDao;
import com.xdev.vab.booking.sys.dao.mapper.ArtistAccountMapper;
import com.xdev.vab.booking.sys.dao.mapper.CustomerMapper;
import com.xdev.vab.booking.sys.model.ArtistAccount;
import com.xdev.vab.booking.sys.model.Customer;
import com.xdev.vab.booking.sys.service.dto.account.LoginDto;
import com.xdev.vab.booking.sys.service.dto.account.RegisterDto;
import com.xdev.vab.booking.sys.service.impl.StoreProcedureCaller;
import com.xdev.vab.booking.utils.CryptoUtils;
import oracle.jdbc.OracleTypes;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Repository
public class AccountDaoImpl implements AccountDao {
    private final DataSource dataSource;

    @Autowired
    public AccountDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public ArtistAccount artistLogin(String requestId, LoginDto loginDto) {
        String refreshToken = UUID.randomUUID().toString();
        Date refreshExpiredTime = DateUtils.addDays(new Date(), 15);

        StoreProcedureCaller caller = new StoreProcedureCaller(dataSource, requestId, "PKG_ACCOUNT", "ARTIST_LOGIN");
        Map<String, Object> result = caller.register("P_CODE", OracleTypes.INTEGER)
                .register("P_DETAIL", OracleTypes.VARCHAR)
                .setValue("P_USERNAME", loginDto.getUserName())
                .setValue("P_PASSWORD", CryptoUtils.encodeMD5(loginDto.getPassword()))
                .setValue("P_REFRESH_TOKEN", refreshToken)
                .setValue("P_EXPIRED_TIME", refreshExpiredTime)
                .register("P_USER_INFO", OracleTypes.CURSOR)
                .returningResultSet("P_USER_INFO", new ArtistAccountMapper())
                .register("P_EMAIL", OracleTypes.VARCHAR)
                .executeOnError("P_CODE", "P_DETAIL");
        ArtistAccount artistAccount = ((List<ArtistAccount>) result.get("P_USER_INFO")).get(0);
        artistAccount.setRefreshToken(refreshToken);
        artistAccount.setRefreshTokenExpired(refreshExpiredTime);
        artistAccount.setEmail((String) result.get("P_EMAIL"));
        return artistAccount;
    }

    @Override
    public void register(String requestId, RegisterDto registerDto) {
        StoreProcedureCaller caller = new StoreProcedureCaller(dataSource, requestId, "PKG_ACCOUNT", "ARTIST_REGISTER");
        Map<String, Object> result =
                caller
                        .register("P_CODE", OracleTypes.INTEGER)
                        .register("P_DETAIL", OracleTypes.VARCHAR)
                        .setValue("P_USERNAME", registerDto.getUserName())
                        .setValue("P_PASSWORD", registerDto.getPassword())
                        .executeOnError("P_CODE", "P_DETAIL");
    }

    @Override
    public Customer customerLogin(String requestId, LoginDto loginDto) {
        String refreshToken = UUID.randomUUID().toString();
        Date refreshTokenExpired = DateUtils.addDays(new Date(), 15);

        StoreProcedureCaller caller = new StoreProcedureCaller(dataSource, requestId, "PKG_ACCOUNT", "CUSTOMER_LOGIN");
        Map<String, Object> result =
                caller
                        .register("P_CODE", OracleTypes.INTEGER)
                        .register("P_DETAIL", OracleTypes.VARCHAR)
                        .setValue("P_USERNAME", loginDto.getUserName())
                        .setValue("P_PASSWORD", CryptoUtils.encodeMD5(loginDto.getPassword()))
                        .setValue("P_REFRESH_TOKEN", refreshToken)
                        .setValue("P_EXPIRED_TIME", refreshTokenExpired)
                        .register("P_CUSTOMER_INFO", OracleTypes.CURSOR)
                        .returningResultSet("P_CUSTOMER_INFO", new CustomerMapper())
                        .executeOnError("P_CODE", "P_DETAIL");
        return ((List<Customer>) result.get("P_CUSTOMER_INFO")).get(0);
    }

    @Override
    public ArtistAccount refreshTokenForArtist(String requestId, String refreshToken) {
        StoreProcedureCaller caller = new StoreProcedureCaller(dataSource, requestId, "pkg_artist", "REFRESH_TOKEN_FOR_ARTIST");
        Map<String, Object> result =
                caller
                        .register("P_CODE", OracleTypes.INTEGER)
                        .register("P_DETAIL", OracleTypes.VARCHAR)
                        .setValue("P_REFRESH_TOKEN", refreshToken)
                        .register("P_ACCOUNT_INFO", OracleTypes.CURSOR)
                        .returningResultSet("P_ACCOUNT_INFO", new ArtistAccountMapper())
                        .executeOnError();
        return ((List<ArtistAccount>) result.get("P_ACCOUNT_INFO")).get(0);
    }

    @Override
    public Customer refreshTokenForCustomer(String requestId, String refreshToken) {
        StoreProcedureCaller caller = new StoreProcedureCaller(dataSource, requestId, "pkg_artist", "REFRESH_TOKEN_FOR_CUSTOMER");
        Map<String, Object> result =
                caller
                        .register("P_CODE", OracleTypes.INTEGER)
                        .register("P_DETAIL", OracleTypes.VARCHAR)
                        .setValue("P_REFRESH_TOKEN", refreshToken)
                        .register("P_ACCOUNT_INFO", OracleTypes.CURSOR)
                        .returningResultSet("P_ACCOUNT_INFO", new CustomerMapper())
                        .executeOnError();
        return ((List<Customer>) result.get("P_ACCOUNT_INFO")).get(0);
    }

    @Override
    public void customerChangePass(String requestId, LoginDto loginDto, Auth auth) {
        StoreProcedureCaller caller = new StoreProcedureCaller(dataSource, requestId, "PKG_ACCOUNT", "customer_change_pass");
        Map<String, Object> result =
                caller
                        .register("p_code", OracleTypes.INTEGER)
                        .register("p_detail", OracleTypes.VARCHAR)
                        .setValue("p_userid", auth.getId())
                        .setValue("p_passold", CryptoUtils.encodeMD5(loginDto.getPass_old()))
                        .setValue("p_passnew", CryptoUtils.encodeMD5(loginDto.getPassword()))
                        .executeOnError("p_code", "p_detail");
    }

    @Override
    public void artistChangePass(String requestId, LoginDto loginDto, Auth auth) {
        StoreProcedureCaller caller = new StoreProcedureCaller(dataSource, requestId, "PKG_ACCOUNT", "artist_change_pass");
        Map<String, Object> result =
                caller
                        .register("p_code", OracleTypes.INTEGER)
                        .register("p_detail", OracleTypes.VARCHAR)
                        .setValue("p_userid", auth.getId())
                        .setValue("p_passold", CryptoUtils.encodeMD5(loginDto.getPass_old()))
                        .setValue("p_passnew", CryptoUtils.encodeMD5(loginDto.getPassword()))
                        .executeOnError("p_code", "p_detail");
    }
}


