package com.xdev.vab.booking.sys.service.dto.account;

import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
@ToString
public class LoginDto {
    @NotBlank
    @Length(min = 5)
    private String userName;

    @NotBlank
    @Length(min = 5)
    private String password;

    @NotBlank
    @Length(min = 5)
    private String pass_old;
}
