package com.xdev.vab.booking.sys.dao.impl;

import com.xdev.vab.booking.common.Auth;
import com.xdev.vab.booking.common.PageData;
import com.xdev.vab.booking.sys.dao.ArtistDao;
import com.xdev.vab.booking.sys.dao.mapper.*;
import com.xdev.vab.booking.sys.model.*;
import com.xdev.vab.booking.sys.service.dto.artist.*;
import com.xdev.vab.booking.sys.service.impl.StoreProcedureCaller;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Repository
public class ArtistDaoImpl implements ArtistDao {
    private final DataSource dataSource;

    @Autowired
    public ArtistDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public PageData<ArtistDto> getList(String requestId, Integer pageIndex, Integer pageSize, Auth auth, String artisticName) {
        StoreProcedureCaller caller = new StoreProcedureCaller(dataSource, requestId, "pkg_artist", "GET_LIST_ARTIST");
        Map<String, Object> result =
                caller
                        .register("P_CODE", OracleTypes.INTEGER)
                        .register("P_DETAIL", OracleTypes.VARCHAR)
                        .setValue("P_PAGE_INDEX", pageIndex)
                        .setValue("P_PAGE_SIZE", pageSize)
                        .register("P_BEGIN_INDEX", OracleTypes.INTEGER)
                        .register("P_END_INDEX", OracleTypes.INTEGER)
                        .register("P_TOTAL_RECORD", OracleTypes.INTEGER)
                        .register("P_TOTAL_PAGE", OracleTypes.INTEGER)
                        .register("P_LIST_ARTIST", OracleTypes.CURSOR)
                        .setValue("P_USER_ID",  auth == null ? null : auth.getId())
                        .setValue("P_ARTISTIC_NAME", artisticName)
                        .returningResultSet("P_LIST_ARTIST", new ArtistDtoMapper())
                        .executeOnError();
        PageData<ArtistDto> pageData = new PageData<>();
        pageData.setPageIndex(pageIndex);
        pageData.setPageSize(pageSize);
        pageData.setBeginIndex((int) result.get("P_BEGIN_INDEX"));
        pageData.setEndIndex((int) result.get("P_END_INDEX"));
        pageData.setTotalPages((int) result.get("P_TOTAL_PAGE"));
        pageData.setTotalRecords((int) result.get("P_TOTAL_RECORD"));
        pageData.setData((List<ArtistDto>) result.get("P_LIST_ARTIST"));
        return pageData;
    }

    @Override
    public ArtistProfile getProfile(String requestId, int artistId, Auth auth) {
        StoreProcedureCaller caller = new StoreProcedureCaller(dataSource, requestId, "pkg_artist", "GET_PROFILE");
        Map<String, Object> result =
                caller
                        .register("P_CODE", OracleTypes.INTEGER)
                        .register("P_DETAIL", OracleTypes.VARCHAR)
                        .setValue("P_ARTIST_ID", artistId)
                        .register("P_ARTIST", OracleTypes.CURSOR)
                        .returningResultSet("P_ARTIST", new ArtistMapper())
                        .register("P_ARTIST_BANNER", OracleTypes.CURSOR)
                        .returningResultSet("P_ARTIST_BANNER", new ArtistBannerMapper())
                        .register("P_ARTIST_STORY", OracleTypes.CURSOR)
                        .returningResultSet("P_ARTIST_STORY", new ArtistStoryMapper())
                        .register("P_PRODUCT", OracleTypes.CURSOR)
                        .returningResultSet("P_PRODUCT", new ArtistProductMapper())
                        .setValue("P_AUTH_ID", auth == null ? null : auth.getId())
                        .setValue("P_TYPE", auth == null ? null : auth.getRoles().get(0))
                        .register("P_ROLE", OracleTypes.VARCHAR)
                        .register("P_IS_FAVORITE", OracleTypes.INTEGER)
                        .executeOnError("P_CODE", "P_DETAIL");

        Artist artist = ((List<Artist>) result.get("P_ARTIST")).get(0);
        List<ArtistBanner> banners = (List<ArtistBanner>) result.get("P_ARTIST_BANNER");
        List<ArtistStory> stories = (List<ArtistStory>) result.get("P_ARTIST_STORY");
        List<ArtistProduct> products = (List<ArtistProduct>) result.get("P_PRODUCT");


        ArtistProfile artistProfile = new ArtistProfile(artist, banners, stories, products);
        artistProfile.setRoles(Collections.singletonList((String) result.get("P_ROLE")));
        artistProfile.setIsFavorite((Integer) result.get("P_IS_FAVORITE"));
        return artistProfile;
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void updateSocialEmbedded(String requestId, ArtistSocialEmbeddedDto artistSocialEmbeddedDto) {
        StoreProcedureCaller caller = new StoreProcedureCaller(dataSource, requestId, "pkg_artist", "UPDATE_SOCIAL_EMBEDDED");
        Map<String, Object> result =
                caller
                        .register("P_CODE", OracleTypes.INTEGER)
                        .register("P_DETAIL", OracleTypes.VARCHAR)
                        .setValue("P_ARTIST_ID", artistSocialEmbeddedDto.getArtistId())
                        .setValue("P_SOCIAL_EMBEDDED", artistSocialEmbeddedDto.getSocialEmbedded())
                        .executeOnError("P_CODE", "P_DETAIL");
    }


    @Override
    @Transactional(rollbackOn = Exception.class)
    public void addProduct(String requestId, ArtistProductDto productDto) {
        StoreProcedureCaller caller = new StoreProcedureCaller(dataSource, requestId, "pkg_artist", "ADD_PRODUCT");
        caller
                .register("P_CODE", OracleTypes.INTEGER)
                .register("P_DETAIL", OracleTypes.VARCHAR)
                .setValue("P_ARTIST_ID", productDto.getArtistId())
                .setValue("P_PRODUCT_CODE", productDto.getCode())
                .setValue("P_NAME", productDto.getName())
                .setValue("P_EMBEDDED_URL", productDto.getEmbeddedUrl())
                .setValue("P_IMAGE_URL", productDto.getImageUrl())
                .setValue("P_STATUS", productDto.getStatus())
                .executeOnError("P_CODE", "P_DETAIL");
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void updateProduct(String requestId, ArtistProductDto productDto) {
        StoreProcedureCaller caller = new StoreProcedureCaller(dataSource, requestId, "pkg_artist", "UPDATE_PRODUCT");
        caller
                .register("P_CODE", OracleTypes.INTEGER)
                .register("P_DETAIL", OracleTypes.VARCHAR)
                .setValue("P_ARTIST_ID", productDto.getArtistId())
                .setValue("P_ID", productDto.getId())
                .setValue("P_PRODUCT_CODE", productDto.getCode())
                .setValue("P_NAME", productDto.getName())
                .setValue("P_EMBEDDED_URL", productDto.getEmbeddedUrl())
                .setValue("P_IMAGE_URL", productDto.getImageUrl())
                .setValue("P_STATUS", productDto.getStatus())
                .executeOnError("P_CODE", "P_DETAIL");
    }


    @Override
    @Transactional(rollbackOn = Exception.class)
    public void deleteProduct(String requestId, int artistId, int productId) {
        StoreProcedureCaller caller = new StoreProcedureCaller(dataSource, requestId, "pkg_artist", "DELETE_PRODUCT");
        caller
                .register("P_CODE", OracleTypes.INTEGER)
                .register("P_DETAIL", OracleTypes.VARCHAR)
                .setValue("P_ARTIST_ID", artistId)
                .setValue("P_ID", productId)
                .executeOnError("P_CODE", "P_DETAIL");
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void favorite(String requestId, int artistId, Auth auth) {
        StoreProcedureCaller caller = new StoreProcedureCaller(dataSource, requestId, "pkg_artist", "FAVORITE");
        caller
                .register("P_CODE", OracleTypes.INTEGER)
                .register("P_DETAIL", OracleTypes.VARCHAR)
                .setValue("P_ARTIST_ID", artistId)
                .setValue("P_AUTH_ID", auth.getId())
                .setValue("P_ROLE", String.join(",", auth.getRoles()))
                .executeOnError();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void unFavorite(String requestId, int artistId, Auth auth) {
        StoreProcedureCaller caller = new StoreProcedureCaller(dataSource, requestId, "pkg_artist", "UN_FAVORITE");
        caller
                .register("P_CODE", OracleTypes.INTEGER)
                .register("P_DETAIL", OracleTypes.VARCHAR)
                .setValue("P_ARTIST_ID", artistId)
                .setValue("P_AUTH_ID", auth.getId())
                .setValue("P_ROLE", String.join(",", auth.getRoles()))
                .executeOnError();
    }
}
