
package com.xdev.vab.booking.sys.dao.mapper;

import com.xdev.vab.booking.sys.model.Customer;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerMapper implements RowMapper<Customer> {
    @Override
    public Customer mapRow(ResultSet resultSet, int i) throws SQLException {
        Customer customer = new Customer();
        customer.setId(resultSet.getInt("ID"));
        customer.setCode(resultSet.getString("CODE"));
        customer.setUserName(resultSet.getString("USER_NAME"));
        customer.setFullName(resultSet.getString("FULL_NAME"));
        customer.setCompanyName(resultSet.getString("COMPANY_NAME"));
        customer.setTaxCode(resultSet.getString("TAX_CODE"));
        customer.setEmail(resultSet.getString("EMAIL"));
        customer.setCustomerLevel(resultSet.getInt("CUSTOMER_LEVEL"));
        customer.setStatus(resultSet.getInt("STATUS"));
        customer.setPhone(resultSet.getString("PHONE"));
        customer.setPassword(resultSet.getString("PASSWORD"));
        customer.setCreateTime(resultSet.getDate("CREATE_TIME"));
        customer.setCreateBy(resultSet.getString("CREATE_BY"));
        customer.setUpdateTime(resultSet.getDate("UPDATE_TIME"));
        customer.setUpdateBy(resultSet.getString("UPDATE_BY"));
        customer.setRefreshToken(resultSet.getString("REFRESH_TOKEN"));
        customer.setRefreshTokenExpired(resultSet.getDate("REFRESH_TOKEN_EXPIRED"));
        customer.setAddress(resultSet.getString("ADDRESS"));
        customer.setType(resultSet.getString("TYPE"));
        customer.setRepresentative(resultSet.getString("REPRESENTATIVE"));
        customer.setPosition(resultSet.getString("POSITION"));
        return customer;
    }
}

