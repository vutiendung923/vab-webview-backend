package com.xdev.vab.booking.sys.dao;

import com.xdev.vab.booking.common.PageData;
import com.xdev.vab.booking.sys.model.Post;
import com.xdev.vab.booking.sys.model.PostCategory;

import java.util.List;

public interface PostDao {
    PageData<Post> getList(String requestId, Integer langId, Integer pageIndex, Integer pageSize, Integer categoryId);

    Post getDetail(String requestId, Integer langId, Integer id);

    List<PostCategory> getListCategory(String requestId, Integer langId);
}
