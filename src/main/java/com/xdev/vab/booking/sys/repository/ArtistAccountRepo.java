package com.xdev.vab.booking.sys.repository;

import com.xdev.vab.booking.sys.model.ArtistAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArtistAccountRepo extends JpaRepository<ArtistAccount, Integer> {
}
