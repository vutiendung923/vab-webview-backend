package com.xdev.vab.booking.sys.dao;

import com.xdev.vab.booking.common.Auth;
import com.xdev.vab.booking.sys.model.ArtistAccount;
import com.xdev.vab.booking.sys.model.Customer;
import com.xdev.vab.booking.sys.service.dto.account.LoginDto;
import com.xdev.vab.booking.sys.service.dto.account.RegisterDto;

public interface AccountDao {
    ArtistAccount artistLogin(String requestId, LoginDto loginDto);

    void register(String requestId, RegisterDto registerDto);

    Customer customerLogin(String requestId, LoginDto loginDto);

    ArtistAccount refreshTokenForArtist(String requestId, String refreshToken);

    Customer refreshTokenForCustomer(String requestId, String refreshToken);

    void customerChangePass(String requestId, LoginDto loginDto, Auth auth);

    void artistChangePass(String requestId, LoginDto loginDto, Auth auth);
}
