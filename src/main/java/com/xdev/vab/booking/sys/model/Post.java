package com.xdev.vab.booking.sys.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Post {
    private Integer id;
    private String image;
    private Integer status;
    private Date createTime;
    private String createBy;
    private Date updateTime;
    private String updateBy;
    private Integer categoryId;
    private Integer viewNumber;
    private String name;
    private String title;
    private Integer langId;
    private String content;
}
