package com.xdev.vab.booking.sys.service.dto.home;

import com.xdev.vab.booking.sys.model.ArtistProduct;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProductHomeDto {
    private Integer id;
    private String name;
    private String embeddedUrl;
    private String imageUrl;
    private Integer numIndex;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Integer id;
        private String name;
        private String embeddedUrl;
        private String imageUrl;
        private Integer numIndex;

        public Builder artistProduct(ArtistProduct artistProduct) {
            this.id = artistProduct.getId();
            this.name = artistProduct.getName();
            this.embeddedUrl = artistProduct.getEmbeddedUrl();
            this.imageUrl = artistProduct.getImageUrl();
            this.numIndex = artistProduct.getNumIndex();
            return this;
        }

        public ProductHomeDto build() {
            return new ProductHomeDto(id, name, embeddedUrl, imageUrl, numIndex);
        }
    }
}
