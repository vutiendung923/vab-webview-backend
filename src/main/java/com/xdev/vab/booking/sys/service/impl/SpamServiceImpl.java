package com.xdev.vab.booking.sys.service.impl;

import com.xdev.vab.booking.sys.service.SpamService;
import org.ehcache.Cache;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

@Service
public class SpamServiceImpl implements SpamService {
    private final Cache<HttpSession, Long> spamManager;

    public SpamServiceImpl(@Qualifier("getSpamManager") Cache<HttpSession, Long> spamManager) {
        this.spamManager = spamManager;
    }


    @Override
    public boolean spamPerSecond(HttpSession session) {
        if (!spamManager.containsKey(session)) {
            spamManager.put(session, System.currentTimeMillis());
            return false;
        }

        Long lastAccess = spamManager.get(session);
        if (lastAccess + 1000 < System.currentTimeMillis()) {
            spamManager.put(session, System.currentTimeMillis());
            return false;
        }

        return true;
    }
}
