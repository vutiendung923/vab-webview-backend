package com.xdev.vab.booking.sys.service;

import com.xdev.vab.booking.common.Auth;
import com.xdev.vab.booking.common.PageData;
import com.xdev.vab.booking.sys.service.dto.ProductDto;
import com.xdev.vab.booking.sys.service.dto.artist.*;
import org.springframework.data.domain.Page;
import org.springframework.data.relational.core.sql.In;

import java.util.List;

public interface ArtistService {
    PageData<ArtistDto> getList(String requestId, Integer pageIndex, Integer pageSize, Auth auth, String artisticName);

    ArtistProfile getProfile(String requestId, Auth auth, int artistId, ArtistDto artistDto);

    ArtistDictionaryDto getDictionary(Integer artistId );

    void updateSocialEmbedded(String requestId, ArtistSocialEmbeddedDto artistSocialEmbeddedDto);

    void updateStory(String requestId, ArtistStoryDto artistStoryDto, Auth auth);

    void addStory(String requestId, ArtistStoryDto artistStoryDto, Auth auth);

    void deleteStory(String requestId, Integer id);

    void addProduct(String requestId, ArtistProductDto artistProductDto);

    void updateProduct(String requestId, ArtistProductDto productDto);

    void deleteProduct(String requestId, int artistId, int productId);

    void favorite(String requestId, Integer artistId, Auth auth);

    void unFavorite(String requestId, Integer artistId, Auth auth);

    void addBanner(String requestId, ArtistBannerDto artistBannerDto, Auth auth);

    void updateBanner(String requestId, ArtistBannerDto artistBannerDto, Auth auth);

    void delete(String requestId, Integer id);

    void pushProduct(String requestId, ProductDto productDto);

    PageData<ArtistDto> getName(Integer pageIndex, Integer pageSize, String artisticName);

    void updateDescription(ArtistDescriptionDto artistDescriptionDto);


}
