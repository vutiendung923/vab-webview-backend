package com.xdev.vab.booking.sys.service;


import com.xdev.vab.booking.sys.service.dto.home.BannerDto;
import com.xdev.vab.booking.sys.service.dto.home.PartnerDto;
import com.xdev.vab.booking.sys.service.dto.home.ProductHomeDto;

import java.util.List;

public interface HomeService {
    List<PartnerDto> getListPartner();
    List<BannerDto> getListBanner();
    List<ProductHomeDto> getListProduct(Integer salientStatus);
}
