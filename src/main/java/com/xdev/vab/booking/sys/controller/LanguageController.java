package com.xdev.vab.booking.sys.controller;

import com.xdev.vab.booking.common.ResponseEntity;
import com.xdev.vab.booking.common.ResponseTypes;
import com.xdev.vab.booking.config.inject.CurrentRequestId;
import com.xdev.vab.booking.sys.service.LanguageService;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.Date;

@Component
@Path("language")
@Valid
public class LanguageController {

    private final LanguageService languageService;

    @CurrentRequestId
    private String requestId;

    public LanguageController(LanguageService languageService) {
        this.languageService = languageService;
    }

    @GET
    public Response getList(){
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS, languageService.getList(requestId))).build();
    }
}
