package com.xdev.vab.booking.sys.repository;

import com.xdev.vab.booking.sys.model.Artist;
import com.xdev.vab.booking.sys.model.Product;
import com.xdev.vab.booking.sys.service.dto.ProductDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.relational.core.sql.In;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepo extends JpaRepository<Product, Integer> {
    @Modifying
    @Query("update Product p set p.status = 1 where  p.artist.id = :artistId")
    void removeProductHighlight(@Param("artistId") Integer artistId);
}
