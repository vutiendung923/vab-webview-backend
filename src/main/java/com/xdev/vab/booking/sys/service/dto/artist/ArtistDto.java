package com.xdev.vab.booking.sys.service.dto.artist;

import com.xdev.vab.booking.sys.model.Artist;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ArtistDto {
    private int id;
    private String encodeId;
    private String code;
    private String artistName;    // Nghệ danh
    private String avatar;  // ảnh đại diện
    private String fullName;    // Tên đầy đủ của nghệ sỹ
    private String works;
    private Integer rank;
    private Integer  favoriteStatus;
    private Integer viewArtist;
    public ArtistDto() {

    }

    public ArtistDto(Artist artist) {
       this.id = artist.getId();
       this.encodeId = artist.getEncodeId();
       this.code = artist.getCode();
       this.artistName = artist.getArtisticName();
       this.avatar = artist.getAvatar();
       this.fullName = artist.getFullName();
       this.rank = artist.getArtistLevel().getId();
       this.favoriteStatus = artist.getFavoriteStatus();
       this.viewArtist = artist.getViewArtist();
    }
}
