package com.xdev.vab.booking.sys.service.dto.artist;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class ArtistStoryDto {
    private Integer id;
    private Integer artistId;
    private String title;
    private String content;
    private String imageUrl;
    private Integer status;
    private String createBy;
    private Date createTime;
    private String updateBy;
    private Date updateTime;
}
