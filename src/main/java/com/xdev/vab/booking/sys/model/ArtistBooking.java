package com.xdev.vab.booking.sys.model;
import com.xdev.vab.booking.common.Schema;
import com.xdev.vab.booking.sys.service.dto.booking.ArtistBookingDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
@Entity
@Data
@ToString
@Table(name = "ARTIST_BOOKING", schema = Schema.BOOKING)
@AllArgsConstructor
@NoArgsConstructor
@SequenceGenerator(name = "SEQ_ARTIST_BOOKING", sequenceName = "SEQ_ARTIST_BOOKING", allocationSize = 1, schema = Schema.BOOKING)
public class ArtistBooking {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ARTIST_BOOKING")
    @Column(name = "ID")
    private Integer id;
    private Date fromTime;
    private Date toTime;
    private Integer status;
    private String content;
    private String address;
    private String createBy;
    private Date createTime;
    private String updateBy;
    private Date updateTime;
    private String description;
    private String expectFee;
    private String phone;
    private Integer performStatus;

    @ManyToOne
    @JoinColumn(name = "CUSTOMER_ID")
    private Customer customer;

    @Column(name = "BOOKING_CODE")
    private Integer bookingCode;

    @ManyToOne
    @JoinColumn(name = "ARTIST_ID")
    private Artist artist;

}
