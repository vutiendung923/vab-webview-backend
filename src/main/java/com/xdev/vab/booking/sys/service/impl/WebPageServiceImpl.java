package com.xdev.vab.booking.sys.service.impl;

import com.xdev.vab.booking.sys.dao.ConfigDao;
import com.xdev.vab.booking.sys.model.PageConfig;
import com.xdev.vab.booking.sys.model.WebPage;
import com.xdev.vab.booking.sys.repository.ConfigRepo;
import com.xdev.vab.booking.sys.root.PageConfig_;
import com.xdev.vab.booking.sys.root.WebPage_;
import com.xdev.vab.booking.sys.service.WebPageService;
import com.xdev.vab.booking.sys.service.dto.WebPage.PageConfigDetailDto;
import com.xdev.vab.booking.sys.service.dto.WebPage.WebPageDto;
import com.xdev.vab.booking.utils.QueryUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Join;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class WebPageServiceImpl implements WebPageService {

    private final ConfigDao configDao;
    private final ConfigRepo configRepo;

    public WebPageServiceImpl(ConfigDao configDao, ConfigRepo configRepo) {

        this.configDao = configDao;
        this.configRepo = configRepo;
    }

    @Override
    @Transactional
    public List<WebPageDto> getListWebPage(String requestId, Integer menuId, Integer langId) {
        return configDao.getListWebPage(requestId, menuId, langId);
    }

    @Override
    public List<PageConfigDetailDto> getDetail(String requestId, Integer pageId, String path, Integer langId) {
        Specification<PageConfig> specification = (root, cq, cb) -> {
            Join<PageConfig, WebPage> configWebPageJoin = root.join(PageConfig_.WEB_PAGE);
         return cb.and(
                 QueryUtils.buildEqFilter(root, cb, configWebPageJoin.get(WebPage_.ID), pageId),
                 QueryUtils.buildLikeFilter(root, cb, path, configWebPageJoin.get(WebPage_.URL)),
                 QueryUtils.buildEqFilter(root, cb, configWebPageJoin.get(WebPage_.LANG_ID), langId)
            );
        };
        List<PageConfigDetailDto> pageConfigDtos = configRepo.findAll(specification).stream()
                .map(pageConfig -> PageConfigDetailDto.builder().pageConfigDetail(pageConfig).build())
                .collect(Collectors.toList());
        return pageConfigDtos;
    }
}
