package com.xdev.vab.booking.sys.service;

import com.xdev.vab.booking.common.Auth;
import com.xdev.vab.booking.common.PageData;
import com.xdev.vab.booking.sys.service.dto.booking.*;

public interface BookingService {

    ArtistBookingDtos setBooking(ArtistBookingDtos artistBookingDtos, Auth auth);

    PageData<ArtistBookingDto> getBooked(Integer pageIndex, Integer pageSize, Integer status,
                                         String phone, String address,
                                         Integer custId, Integer artistId, String fromTime, String toTime,
                                         Integer performStatus);

    void setArtistVerify(ArtistBookingArtistVerifyDto artistBookingArtistVerifyDto, Auth auth);

    void deleteRequest(Integer id);

    void setPerformVerify(ArtistBookingPerformVerifyDto artistBookingPerformVerifyDto, Auth auth);
}
