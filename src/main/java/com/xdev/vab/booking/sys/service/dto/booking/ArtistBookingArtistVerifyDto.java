package com.xdev.vab.booking.sys.service.dto.booking;


import com.xdev.vab.booking.sys.model.ArtistBooking;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ArtistBookingArtistVerifyDto {
    private Integer id;
    private Integer artistId;
    private Integer status;
    private Integer performStatus;
}
