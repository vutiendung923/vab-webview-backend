
package com.xdev.vab.booking.sys.dao.mapper;

import com.xdev.vab.booking.sys.model.ArtistBanner;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ArtistBannerMapper implements RowMapper<ArtistBanner> {
    @Override
    public ArtistBanner mapRow(ResultSet resultSet, int i) throws SQLException {
        ArtistBanner artistBanner = new ArtistBanner();
        artistBanner.setId(resultSet.getInt("ID"));
        artistBanner.setBannerUrl(resultSet.getString("BANNER_URL"));
        artistBanner.setStatus(resultSet.getInt("STATUS"));
        artistBanner.setNumIndex(resultSet.getInt("NUM_INDEX"));
        return artistBanner;
    }
}
