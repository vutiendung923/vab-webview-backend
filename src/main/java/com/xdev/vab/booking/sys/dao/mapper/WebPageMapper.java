package com.xdev.vab.booking.sys.dao.mapper;

import com.xdev.vab.booking.sys.model.WebPage;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class WebPageMapper implements RowMapper<WebPage> {
    @Override
    public WebPage mapRow(ResultSet resultSet, int i) throws SQLException {
        WebPage webPage = new WebPage();
        webPage.setId(resultSet.getInt("ID"));
        webPage.setName(resultSet.getString("NAME"));
        webPage.setUrl(resultSet.getString("URL"));
        webPage.setStatus(resultSet.getInt("STATUS"));
        webPage.setLangId(resultSet.getInt("LANG_ID"));
        webPage.setNumIndex(resultSet.getInt("NUM_INDEX"));
        return webPage;
    }
}
