package com.xdev.vab.booking.sys.dao.mapper;

import com.xdev.vab.booking.sys.model.ArtistProduct;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ArtistProductMapper implements RowMapper<ArtistProduct> {
    @Override
    public ArtistProduct mapRow(ResultSet resultSet, int i) throws SQLException {
        ArtistProduct artistProduct = new ArtistProduct();
        artistProduct.setId(resultSet.getInt("ID"));
        artistProduct.setArtistId(resultSet.getInt("ARTIST_ID"));
        artistProduct.setCode(resultSet.getString("CODE"));
        artistProduct.setName(resultSet.getString("NAME"));
        artistProduct.setEmbeddedUrl(resultSet.getString("EMBEDDED_URL"));
        artistProduct.setImageUrl(resultSet.getString("IMAGE_URL"));
        artistProduct.setStatus(resultSet.getInt("STATUS"));
        artistProduct.setNumIndex(resultSet.getInt("NUM_INDEX"));
        artistProduct.setCreateTime(resultSet.getDate("CREATE_TIME"));
        artistProduct.setCreateBy(resultSet.getString("CREATE_BY"));
        artistProduct.setUpdateBy(resultSet.getString("UPDATE_BY"));
        artistProduct.setUpdateTime(resultSet.getDate("UPDATE_TIME"));
        return artistProduct;
    }
}
