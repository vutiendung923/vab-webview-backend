package com.xdev.vab.booking.sys.controller;

import com.xdev.vab.booking.common.Auth;
import com.xdev.vab.booking.common.ResponseEntity;
import com.xdev.vab.booking.common.ResponseTypes;
import com.xdev.vab.booking.common.Roles;
import com.xdev.vab.booking.config.inject.CurrentAuth;
import com.xdev.vab.booking.config.inject.CurrentIp;
import com.xdev.vab.booking.config.inject.CurrentRequestId;
import com.xdev.vab.booking.sys.service.BookingService;
import com.xdev.vab.booking.sys.service.dto.booking.*;
import org.springframework.stereotype.Component;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.Date;

@Component
@Path("booking")
@Valid
public class BookingController {

    private final BookingService bookingService;

    @CurrentRequestId
    private String requestId;

    @CurrentAuth
    private Auth auth;

    @CurrentIp
    private String ip;

    public BookingController(BookingService customerService) {
        this.bookingService = customerService;
    }

    @RolesAllowed(Roles.CUSTOMER)
    @Path("customer/request")
    @POST
    public Response setBooking(@NotNull ArtistBookingDtos artistBookingDtos){
        ArtistBookingDtos artistBooking = bookingService.setBooking(artistBookingDtos, auth);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS, artistBooking)).build();
    }

    @RolesAllowed(Roles.CUSTOMER)
    @Path("customer/verify")
    @POST
    public Response setCustVerify(){
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @RolesAllowed(Roles.ARTIST)
    @Path("artist/verify")
    @POST
    public Response setArtistVerify(ArtistBookingArtistVerifyDto artistBookingArtistVerifyDto){
        bookingService.setArtistVerify(artistBookingArtistVerifyDto, auth);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @RolesAllowed(Roles.ARTIST)
    @Path("artist/perform")
    @POST
    public Response setPerformVerify(ArtistBookingPerformVerifyDto artistBookingPerformVerifyDto){
        bookingService.setPerformVerify(artistBookingPerformVerifyDto, auth);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @RolesAllowed({Roles.ARTIST, Roles.CUSTOMER})
    @Path("booked")
    @GET
    public Response getBooked(@NotNull @QueryParam("pageIndex") Integer pageIndex,
                              @NotNull @QueryParam("pageSize") Integer pageSize,
                              @QueryParam("custId") Integer custId,
                              @QueryParam("artistId") Integer artistId,
                              @QueryParam("status") Integer status,
                              @QueryParam("phone") String phone,
                              @QueryParam("address") String address,
                              @QueryParam("fromTime") String fromTime,
                              @QueryParam("toTime") String toTime,
                              @QueryParam("performStatus") Integer performStatus) {
        return Response.ok(new  ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS,
                bookingService.getBooked(pageIndex, pageSize, status,
                        phone, address, custId, artistId, fromTime, toTime, performStatus))).build();
    }

    @GET
    @Path("customer/deny")
    @RolesAllowed({Roles.CUSTOMER})
    public Response deleteRequest(@NotNull @QueryParam("id") Integer id) {
        bookingService.deleteRequest(id);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

}
