package com.xdev.vab.booking.sys.service.dto.booking;

import com.xdev.vab.booking.sys.model.ArtistBooking;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class ArtistBookingDtos {
    private Integer id;
    private Integer customerId;
    private Integer artistId;
    private String artistName;
    private String artistFullName;
    private String avatar;
    private Date fromTime;
    private Date toTime;
    private String content;
    private String address;
    private String description;
    private String createBy;
    private Date createTime;
    private String expectFee;
    private String phone;
    private Integer bookingCode;
    private String custName;
    private String mnName;
    private String mnPhone;
    private String mnFbmess;
    private String mnEmail;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Integer id;
        private Integer customerId;
        private Integer artistId;
        private String artistName;
        private String artistFullName;
        private String avatar;
        private Date fromTime;
        private Date toTime;
        private String content;
        private String address;
        private String description;
        private String createBy;
        private Date createTime;
        private String expectFee;
        private String phone;
        private Integer bookingCode;
        private String custName;
        private String mnName;
        private String mnPhone;
        private String mnFbmess;
        private String mnEmail;

         public Builder artistBookings(ArtistBooking artistBooking) {
            this.id = artistBooking.getId();
            if(artistBooking.getArtist() != null) {
                this.artistId = artistBooking.getArtist().getId();
                this.artistName = artistBooking.getArtist().getArtisticName();
                this.avatar = artistBooking.getArtist().getAvatar();
                this.artistFullName = artistBooking.getArtist().getFullName();
            }
            this.fromTime = artistBooking.getFromTime();
            this.toTime = artistBooking.getToTime();
            this.content = artistBooking.getContent();
            this.address = artistBooking.getAddress();
            this.description = artistBooking.getDescription();
            this.createTime = artistBooking.getCreateTime();
            this.createBy = artistBooking.getCreateBy();
            this.expectFee = artistBooking.getExpectFee();
            this.phone = artistBooking.getPhone();
            this.bookingCode = artistBooking.getBookingCode();
            this.customerId = artistBooking.getCustomer().getId();
            this.custName = artistBooking.getCustomer().getFullName();
            if(artistBooking.getArtist().getMnName() == null &&  artistBooking.getArtist().getMnPhone() == null
                    && artistBooking.getArtist().getMnEmail() == null && artistBooking.getArtist().getMnFbmess() == null) {
                this.mnName = artistBooking.getArtist().getFullName();
                this.mnPhone = artistBooking.getArtist().getPhone();
                this.mnEmail = artistBooking.getArtist().getMnEmail();
                this.mnFbmess = artistBooking.getArtist().getFbMessage();
            } else {
                this.mnName = artistBooking.getArtist().getMnName();
                this.mnPhone = artistBooking.getArtist().getMnPhone();
                this.mnEmail = artistBooking.getArtist().getMnEmail();
                this.mnFbmess = artistBooking.getArtist().getMnFbmess();
            }
            return this;
        }

        public ArtistBookingDtos build() {
            return new ArtistBookingDtos(id, customerId, artistId, artistName, avatar, artistFullName, fromTime, toTime, content,
                    address, description, createBy, createTime, expectFee, phone, bookingCode, custName,
                    mnName, mnPhone, mnFbmess, mnEmail);
        }
    }

}
