package com.xdev.vab.booking.sys.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Language {
    private Integer id;
    private String code;
    private String name;
    private Integer status;
    private Integer isDefault;
}
