package com.xdev.vab.booking.sys.service;

import javax.servlet.http.HttpSession;

public interface SpamService {
    boolean spamPerSecond(HttpSession session);
}
