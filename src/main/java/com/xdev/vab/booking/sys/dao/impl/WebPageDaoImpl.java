package com.xdev.vab.booking.sys.dao.impl;

import com.xdev.vab.booking.sys.dao.ConfigDao;
import com.xdev.vab.booking.sys.dao.mapper.WebPageDtoMapper;
import com.xdev.vab.booking.sys.dao.mapper.WebPageMapper;
import com.xdev.vab.booking.sys.service.dto.WebPage.WebPageDto;
import com.xdev.vab.booking.sys.service.impl.StoreProcedureCaller;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

@Repository
public class WebPageDaoImpl implements ConfigDao {
    private final DataSource dataSource;

    @Autowired
    public WebPageDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<WebPageDto> getListWebPage(String requestId, Integer menuId, int langId) {
        StoreProcedureCaller caller = new StoreProcedureCaller(dataSource, requestId, "PKG_CONFIG", "GET_LIST_WEB_PAGE");
        Map<String, Object> result =
                caller
                        .register("P_CODE", OracleTypes.INTEGER)
                        .register("P_DETAIL", OracleTypes.VARCHAR)
                        .setValue("P_LANG_ID", langId)
                        .setValue("P_MENU_ID", menuId)
                        .register("P_WEB_PAGE", OracleTypes.CURSOR)
                        .returningResultSet("P_WEB_PAGE", new WebPageDtoMapper())
                        .executeOnError();
        return (List<WebPageDto>) result.get("P_WEB_PAGE");
    }
}
