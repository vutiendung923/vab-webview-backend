package com.xdev.vab.booking.sys.model;

import com.xdev.vab.booking.common.Schema;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "DIC_ARTIST_LEVEL", schema = Schema.CMS)
public class ArtistLevel {
    @Id
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @OneToMany(mappedBy = "artistLevel")
    private List<Artist> artist;

    public ArtistLevel(String name) {
        this.name = name;
    }

    public ArtistLevel() {

    }
}
