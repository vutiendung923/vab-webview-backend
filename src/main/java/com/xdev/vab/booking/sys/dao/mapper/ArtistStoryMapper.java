package com.xdev.vab.booking.sys.dao.mapper;

import com.xdev.vab.booking.sys.model.ArtistStory;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ArtistStoryMapper implements RowMapper<ArtistStory> {
    @Override
    public ArtistStory mapRow(ResultSet resultSet, int i) throws SQLException {
        ArtistStory artistStory = new ArtistStory();
        artistStory.setId(resultSet.getInt("ID"));
        artistStory.setTitle(resultSet.getString("TITLE"));
        artistStory.setContent(resultSet.getString("CONTENT"));
        artistStory.setImageUrl(resultSet.getString("IMAGE_URL"));
        artistStory.setStatus(resultSet.getInt("STATUS"));
        artistStory.setCreateTime(resultSet.getDate("CREATE_TIME"));
        artistStory.setCreateBy(resultSet.getString("CREATE_BY"));
        artistStory.setUpdateTime(resultSet.getDate("UPDATE_TIME"));
        artistStory.setUpdateBy(resultSet.getString("UPDATE_BY"));
        return artistStory;
    }
}
