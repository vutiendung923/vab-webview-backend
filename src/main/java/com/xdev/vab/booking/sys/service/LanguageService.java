package com.xdev.vab.booking.sys.service;

import com.xdev.vab.booking.sys.model.Language;

import java.util.List;

public interface LanguageService {
    List<Language> getList(String requestId);
}
