package com.xdev.vab.booking.sys.repository;
import com.xdev.vab.booking.sys.model.ArtistBooking;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;



public interface ArtistBookingRepo extends JpaRepository<ArtistBooking, Integer>, JpaSpecificationExecutor<ArtistBooking> {

    @Query("select count(status) from ArtistBooking where status = 0 and  (customer.id = :custId or artist.id = :artistId) ")
    Long statusQuery0(@Param("custId") Integer custId,
                      @Param("artistId") Integer artistId);

    @Query("select count(status) from ArtistBooking where status = 1 and (customer.id = :custId or artist.id = :artistId)")
    Long statusQuery1(@Param("custId") Integer custId,
                      @Param("artistId") Integer artistId);

    @Query("select count(status) from ArtistBooking where status = 2 and (customer.id = :custId or artist.id = :artistId)")
    Long statusQuery2(@Param("custId") Integer custId,
                      @Param("artistId") Integer artistId);

    ArtistBooking findArtistBookingsByIdAndArtistId(Integer id, Integer artistId);
}
