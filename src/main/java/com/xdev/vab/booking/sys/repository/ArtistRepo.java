package com.xdev.vab.booking.sys.repository;

import com.xdev.vab.booking.sys.model.Artist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ArtistRepo extends JpaRepository<Artist, Integer>, JpaSpecificationExecutor<Artist> {
}
