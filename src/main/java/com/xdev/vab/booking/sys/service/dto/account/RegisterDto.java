package com.xdev.vab.booking.sys.service.dto.account;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class RegisterDto {
    private String userName;
    private String password;
}
