package com.xdev.vab.booking.sys.service.dto.artist;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ArtistSocialEmbeddedDto {
    private int artistId;
    private String socialEmbedded;
}
