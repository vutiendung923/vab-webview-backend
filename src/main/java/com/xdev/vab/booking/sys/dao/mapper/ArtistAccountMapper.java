package com.xdev.vab.booking.sys.dao.mapper;

import com.xdev.vab.booking.sys.model.ArtistAccount;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ArtistAccountMapper implements RowMapper<ArtistAccount> {
    @Override
    public ArtistAccount mapRow(ResultSet resultSet, int i) throws SQLException {
        ArtistAccount account = new ArtistAccount();
        account.setId(resultSet.getInt("ID"));
        account.setArtistId(resultSet.getInt("ARTIST_ID"));
        account.setAccount(resultSet.getString("ACCOUNT"));
        account.setPassword(resultSet.getString("PASSWORD"));
        account.setStatus(resultSet.getInt("STATUS"));
        account.setCreateTime(resultSet.getDate("CREATE_TIME"));
        account.setCreateBy(resultSet.getString("CREATE_BY"));
        account.setUpdateBy(resultSet.getString("UPDATE_BY"));
        account.setUpdateTime(resultSet.getDate("UPDATE_TIME"));
        account.setFullName(resultSet.getString("FULL_NAME"));
        account.setAvatar(resultSet.getString("AVATAR"));
        account.setArtistName(resultSet.getString("ARTIST_NAME"));
        return account;
    }
}
