package com.xdev.vab.booking.sys.repository;

import com.xdev.vab.booking.sys.model.ArtistBanner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ArtistBannerRepo extends JpaRepository<ArtistBanner, Integer>, JpaSpecificationExecutor<ArtistBanner> {
    @Modifying
    @Query("update ArtistBanner set numIndex = numIndex+1")
    void updateNumIndex();
}
