package com.xdev.vab.booking.sys.service.dto.account;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class LoginResponse {
    private Object account;
    private String role;
    private String accessToken;
    private String refreshToken;
}
