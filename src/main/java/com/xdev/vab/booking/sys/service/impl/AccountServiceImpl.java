package com.xdev.vab.booking.sys.service.impl;

import com.xdev.vab.booking.common.Auth;
import com.xdev.vab.booking.common.Roles;
import com.xdev.vab.booking.exception.RoleNotFoundException;
import com.xdev.vab.booking.sys.dao.AccountDao;
import com.xdev.vab.booking.sys.model.ArtistAccount;
import com.xdev.vab.booking.sys.model.Customer;
import com.xdev.vab.booking.sys.repository.ArtistAccountRepo;
import com.xdev.vab.booking.sys.repository.CustomerRepo;
import com.xdev.vab.booking.sys.service.AccountService;
import com.xdev.vab.booking.sys.service.JwtService;
import com.xdev.vab.booking.sys.service.dto.account.LoginDto;
import com.xdev.vab.booking.sys.service.dto.account.LoginResponse;
import com.xdev.vab.booking.sys.service.dto.account.RegisterDto;
import com.xdev.vab.booking.utils.CryptoUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class AccountServiceImpl implements AccountService {
    private final AccountDao accountDao;
    private final JwtService jwtService;
    private final CustomerRepo customerRepo;
    private final ArtistAccountRepo artistAccountRepo;

    @Autowired
    public AccountServiceImpl(AccountDao accountDao, JwtService jwtService, CustomerRepo customerRepo, ArtistAccountRepo artistAccountRepo) {
        this.accountDao = accountDao;
        this.jwtService = jwtService;
        this.customerRepo = customerRepo;
        this.artistAccountRepo = artistAccountRepo;
    }

    @Override
    public LoginResponse login(String requestId, LoginDto loginDto) {
        ArtistAccount artistAccount = accountDao.artistLogin(requestId, loginDto);
        Auth auth = new Auth();
        auth.setId(artistAccount.getArtistId());
        auth.setUserName(artistAccount.getAccount());
        auth.setRoles(Collections.singletonList(Roles.ARTIST));
        String token = jwtService.encode(auth, 1);
        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setAccount(artistAccount);
        loginResponse.setRole(Roles.ARTIST);
        loginResponse.setAccessToken(token);
        loginResponse.setRefreshToken(artistAccount.getRefreshToken());
        return loginResponse;
    }

    @Override
    public void customerRegister(String requestId, RegisterDto registerDto) {
        accountDao.register(requestId, registerDto);
    }

    @Override
    public void customerChangePass(String requestId, LoginDto loginDto, Auth auth) {
        accountDao.customerChangePass(requestId,loginDto,auth);
    }

    @Override
    public void artistChangePass(String requestId, LoginDto loginDto, Auth auth) {
        accountDao.artistChangePass(requestId, loginDto, auth);
    }

    @Override
    public LoginResponse customerLogin(String requestId, LoginDto loginDto) {
        Customer customer = accountDao.customerLogin(requestId, loginDto);
        Auth auth = new Auth();
        auth.setId(customer.getId());
        auth.setUserName(customer.getUserName());
        auth.setRoles(Collections.singletonList(Roles.CUSTOMER));
        String token = jwtService.encode(auth, 1);
        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setAccount(customer);
        loginResponse.setRole(Roles.CUSTOMER);
        loginResponse.setAccessToken(token);
        loginResponse.setRefreshToken(customer.getRefreshToken());
        return loginResponse;
    }

    @Override
    public String getAccessToken(String requestId, String refreshToken, String role) {
        switch (role) {
            case Roles.ARTIST:
                ArtistAccount artistAccount = accountDao.refreshTokenForArtist(requestId, refreshToken);
                Auth artistAuth = new Auth();
                artistAuth.setId(artistAccount.getArtistId());
                artistAuth.setUserName(artistAccount.getAccount());
                artistAuth.setRoles(Collections.singletonList(Roles.ARTIST));
                return jwtService.encode(artistAuth, 1);
            case Roles.CUSTOMER:
                Customer customer = accountDao.refreshTokenForCustomer(requestId, refreshToken);
                Auth customerAuth = new Auth();
                customerAuth.setId(customer.getId());
                customerAuth.setUserName(customer.getUserName());
                customerAuth.setRoles(Collections.singletonList(Roles.CUSTOMER));
                return jwtService.encode(customerAuth, 1);
            default:
                throw new RoleNotFoundException(role);
        }
    }
}
