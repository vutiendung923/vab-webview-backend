package com.xdev.vab.booking.sys.repository;

import com.xdev.vab.booking.sys.model.PageConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ConfigRepo extends JpaRepository<PageConfig, Integer>, JpaSpecificationExecutor<PageConfig> {
}
