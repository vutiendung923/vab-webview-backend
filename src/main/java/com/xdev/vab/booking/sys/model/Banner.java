package com.xdev.vab.booking.sys.model;

import com.xdev.vab.booking.common.Schema;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "BANNER", schema = Schema.BOOKING)
public class Banner {
    @Id
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "NUM_INDEX")
    private Integer numIndex;

    @Column(name = "IMAGE")
    private String image;

    @Column(name = "STATUS")
    private Integer status;
}
