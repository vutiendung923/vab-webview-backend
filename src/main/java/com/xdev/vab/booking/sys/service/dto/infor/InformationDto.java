package com.xdev.vab.booking.sys.service.dto.infor;

import com.xdev.vab.booking.sys.model.Information;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class InformationDto {
    private Integer id;
    private String comName;
    private String logo;
    private String email;
    private String website;
    private Integer phone;
    private String address;
    private String fbLink;
    private String twtLink;
    private String insLink;
    private String linked;
    private String snapchat;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Integer id;
        private String comName;
        private String logo;
        private String email;
        private String website;
        private Integer phone;
        private String address;
        private String fbLink;
        private String twtLink;
        private String insLink;
        private String linked;
        private String snapchat;

        public Builder information(Information information) {
            this.id = information.getId();
            this.comName = information.getComName();
            this.logo = information.getLogo();
            this.email = information.getEmail();
            this.website = information.getWebsite();
            this.phone = information.getPhone();
            this.address = information.getAddress();
            this.fbLink = information.getFbLink();
            this.twtLink = information.getTwtLink();
            this.insLink = information.getInsLink();
            this.linked = information.getLinked();
            this.snapchat = information.getSnapchat();
            return this;
        }

        public InformationDto build(){
            return new InformationDto(id, comName, logo, email, website, phone, address,
                    fbLink, twtLink, insLink, linked, snapchat);
        }
    }
}
