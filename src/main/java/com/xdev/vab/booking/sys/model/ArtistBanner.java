package com.xdev.vab.booking.sys.model;

import com.xdev.vab.booking.common.Schema;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Data
@ToString
@Entity
@Table(name = "ARTIST_BANNER", schema = Schema.BOOKING)
@SequenceGenerator(name = "SEQ_ARTIST_BANNER", sequenceName = "SEQ_ARTIST_BANNER", allocationSize = 1, schema = Schema.BOOKING)
public class ArtistBanner {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ARTIST_BANNER")
    @Column(name = "ID")
    private Integer id;

    @Column(name = "BANNER_URL")
    private String bannerUrl;

    @Column(name = "STATUS")
    private int status;

    @Column(name = "NUM_INDEX")
    private Integer numIndex;

    @Column(name = "CREATE_BY")
    private String createBy;

    @Column(name = "CREATE_TIME")
    private Date createTime;

    @Column(name = "UPDATE_BY")
    private String updateBy;

    @Column(name = "UPDATE_TIME")
    private Date updateTime;

    @ManyToOne
    @JoinColumn(name = "ARTIST_ID")
    private Artist artist;
}
