package com.xdev.vab.booking.sys.controller;

import com.xdev.vab.booking.common.Auth;
import com.xdev.vab.booking.common.ResponseEntity;
import com.xdev.vab.booking.common.ResponseTypes;
import com.xdev.vab.booking.config.inject.CurrentAuth;
import com.xdev.vab.booking.config.inject.CurrentRequestId;
import com.xdev.vab.booking.sys.service.InformationService;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.Date;

@Component
@Path("information")
@Valid
public class InforController {
    @CurrentRequestId
    private String requestId;

    @CurrentAuth
    private Auth auth;

    private final InformationService informationService;

    public InforController(InformationService informationService) {
        this.informationService = informationService;
    }

    @GET
    public Response getDetail(@QueryParam("id") Integer id){
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS,
                informationService.getDetail(requestId, id))).build();
    }
}
