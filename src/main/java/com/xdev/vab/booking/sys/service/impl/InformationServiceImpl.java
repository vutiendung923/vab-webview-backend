package com.xdev.vab.booking.sys.service.impl;

import com.xdev.vab.booking.sys.repository.InformationRepo;
import com.xdev.vab.booking.sys.service.InformationService;
import com.xdev.vab.booking.sys.service.dto.infor.InformationDto;
import org.springframework.stereotype.Service;

@Service
public class InformationServiceImpl implements InformationService {

    private final InformationRepo informationRepo;

    public InformationServiceImpl(InformationRepo informationRepo) {
        this.informationRepo = informationRepo;
    }

    @Override
    public InformationDto getDetail(String requestId, Integer id) {
        return InformationDto.builder().information(informationRepo.findById(id).orElseThrow(RuntimeException::new)).build();
    }
}
