package com.xdev.vab.booking.sys.model;

import com.xdev.vab.booking.common.Schema;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "INFORMATION", schema = Schema.CMS)
public class Information {
    @Id
    private Integer id;

    private String comName;
    private String logo;
    private String email;
    private String website;
    private Integer phone;
    private String address;

    @Column(name = "FB_LINK")
    private String fbLink;

    @Column(name = "INS_LINK")
    private String insLink;

    @Column(name = "TWT_LINK")
    private String twtLink;

    @Column(name = "LINKED")
    private String Linked;

    @Column(name = "SNAPCHAT")
    private String snapchat;

}
