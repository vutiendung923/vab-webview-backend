package com.xdev.vab.booking.sys.service.dto.artist;

import com.xdev.vab.booking.sys.model.ArtistProduct;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class ArtistProductDto {
    private int artistId;
    private Integer id;
    private String code;
    private String name;
    private String embeddedUrl;
    private String imageUrl;
    private Integer status;
}
