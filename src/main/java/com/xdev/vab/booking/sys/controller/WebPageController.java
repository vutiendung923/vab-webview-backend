package com.xdev.vab.booking.sys.controller;

import com.xdev.vab.booking.common.ResponseEntity;
import com.xdev.vab.booking.common.ResponseTypes;
import com.xdev.vab.booking.config.inject.CurrentRequestId;
import com.xdev.vab.booking.sys.service.WebPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.Date;

@Path("/config")
@Component
@Valid
@PermitAll
public class WebPageController {
    private final WebPageService webPageService;

    @CurrentRequestId
    private String requestId;

    @Autowired
    public WebPageController(WebPageService webPageService) {
        this.webPageService = webPageService;
    }

    @GET
    @Path("webPage")
    public Response getListWebPage(@QueryParam("langId") @DefaultValue("1") Integer langId,
                                   @QueryParam("menuId") Integer menuId) {
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS, webPageService.getListWebPage(requestId, menuId, langId))).build();
    }

    @GET
    @Path("staticPage")
    public Response getDetail(@QueryParam("pageId") Integer pageId,
                              @QueryParam("path") String path,
                              @QueryParam("langId") Integer langId) {
        return Response.ok(new ResponseEntity(requestId, new Date(),ResponseTypes.SUCCESS,
                webPageService.getDetail(requestId, pageId, path, langId))).build();
    }
}
