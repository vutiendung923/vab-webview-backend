package com.xdev.vab.booking.sys.model;

import com.xdev.vab.booking.common.Schema;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "DIC_QUALIFICATION", schema = Schema.CMS)
public class ArtistQualification {
    @Id
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @OneToMany(mappedBy = "artistQualification")
    private List<Artist> artist;

    public ArtistQualification(String name) {
        this.name = name;
    }

    public ArtistQualification() {

    }
}
