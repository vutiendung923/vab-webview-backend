package com.xdev.vab.booking.sys.controller;

import com.xdev.vab.booking.common.Auth;
import com.xdev.vab.booking.common.ResponseEntity;
import com.xdev.vab.booking.common.ResponseTypes;
import com.xdev.vab.booking.common.Roles;
import com.xdev.vab.booking.config.inject.CurrentAuth;
import com.xdev.vab.booking.config.inject.CurrentIp;
import com.xdev.vab.booking.config.inject.CurrentRequestId;
import com.xdev.vab.booking.sys.service.AccountService;
import com.xdev.vab.booking.sys.service.dto.account.LoginDto;
import com.xdev.vab.booking.sys.service.dto.account.RegisterDto;
import org.glassfish.jersey.internal.inject.Custom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.Date;

@Component
@Path("/account")
@Valid
@PermitAll
public class AccountController {

    private final AccountService accountService;

    @CurrentRequestId
    private String requestId;

    @CurrentAuth
    private Auth auth;

    @CurrentIp
    private String ip;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @Path("artist/login")
    @POST
    public Response artistLogin(@NotNull LoginDto loginDto) {
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS, accountService.login(requestId, loginDto))).build();
    }

    @Path("customer/register")
    @POST
    public Response customerRegister(@NotNull RegisterDto registerDto) {
        accountService.customerRegister(requestId, registerDto);
        return Response.ok().build();
    }

    @Path("customer/login")
    @POST
    public Response customerLogin(@NotNull LoginDto loginDto) {
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS, accountService.customerLogin(requestId, loginDto))).build();
    }

    @Path("getAccessToken")
    @GET
    public Response refreshToken(@NotNull @QueryParam("refreshToken") String refreshToken,
                                 @NotNull @QueryParam("role") String role) {
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS, accountService.getAccessToken(requestId, refreshToken, role))).build();
    }

    @RolesAllowed(Roles.CUSTOMER)
    @Path("customer/password/change")
    @POST
    public Response customerChangePass(LoginDto loginDto) {
        accountService.customerChangePass(requestId, loginDto, auth);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @RolesAllowed(Roles.ARTIST)
    @Path("artist/password/change")
    @POST
    public Response artistChangePass(LoginDto loginDto) {
        accountService.artistChangePass(requestId, loginDto, auth);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }
}
