package com.xdev.vab.booking.sys.service.dto.artist;

import com.xdev.vab.booking.sys.model.*;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class ArtistProfile {
    private Artist artist;
    private List<ArtistBanner> banners;
    private List<ArtistStory> stories;
    private List<ArtistProduct> products;
    private List<String> roles;
    private Integer isFavorite;

    public ArtistProfile(Artist artist, List<ArtistBanner> banners, List<ArtistStory> stories, List<ArtistProduct> products) {
        this.artist = artist;
        this.banners = banners;
        this.stories = stories;
        this.products = products;
    }
}
