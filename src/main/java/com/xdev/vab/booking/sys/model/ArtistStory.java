package com.xdev.vab.booking.sys.model;

import com.xdev.vab.booking.common.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@ToString
@SequenceGenerator(name = "SEQ_ARTIST_STORY", sequenceName = "SEQ_ARTIST_STORY", allocationSize = 1, schema = Schema.BOOKING)
public class ArtistStory extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ARTIST_STORY")
    private Integer id;

    private String title;
    private String content;
    private String imageUrl;
    private int status;

    @ManyToOne
    @JoinColumn(name = "ARTIST_ID")
    private Artist artist;
}
