package com.xdev.vab.booking.sys.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class NationalDictionary {
    private int id;
    private String name;
    private Integer status;
    private Integer numIndex;
}
