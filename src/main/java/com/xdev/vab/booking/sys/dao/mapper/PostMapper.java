package com.xdev.vab.booking.sys.dao.mapper;

import com.xdev.vab.booking.sys.model.Post;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PostMapper implements RowMapper<Post> {
    @Override
    public Post mapRow(ResultSet resultSet, int i) throws SQLException {
        Post post = new Post();
        post.setId(resultSet.getInt("ID"));
        post.setImage(resultSet.getString("IMAGE"));
        post.setStatus(resultSet.getInt("STATUS"));
        post.setCreateTime(resultSet.getDate("CREATE_TIME"));
        post.setCreateBy(resultSet.getString("CREATE_BY"));
        post.setUpdateTime(resultSet.getDate("UPDATE_TIME"));
        post.setUpdateBy(resultSet.getString("UPDATE_BY"));
        post.setCategoryId(resultSet.getInt("CATEGORY_ID"));
        post.setViewNumber(resultSet.getInt("VIEW_NUMBER"));
        post.setName(resultSet.getString("NAME"));
        post.setTitle(resultSet.getString("TITLE"));
        post.setLangId(resultSet.getInt("LANG_ID"));
        post.setContent(resultSet.getString("CONTENT"));
        return post;
    }
}
