package com.xdev.vab.booking.sys.service.impl;

import com.xdev.vab.booking.sys.model.ArtistProduct;
import com.xdev.vab.booking.sys.repository.ArtistProductRepo;
import com.xdev.vab.booking.sys.repository.ArtistRepo;
import com.xdev.vab.booking.sys.repository.BannerRepo;
import com.xdev.vab.booking.sys.repository.PartnerRepo;
import com.xdev.vab.booking.sys.root.ArtistProduct_;
import com.xdev.vab.booking.sys.service.HomeService;
import com.xdev.vab.booking.sys.service.dto.home.BannerDto;
import com.xdev.vab.booking.sys.service.dto.home.PartnerDto;
import com.xdev.vab.booking.sys.service.dto.home.ProductHomeDto;
import com.xdev.vab.booking.utils.QueryUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class HomeServiceImpl implements HomeService {

    private final PartnerRepo partnerRepo;
    private final BannerRepo bannerRepo;
    private final ArtistProductRepo artistProductRepo;

    public HomeServiceImpl(PartnerRepo partnerRepo, BannerRepo bannerRepo, ArtistProductRepo artistProductRepo) {
        this.partnerRepo = partnerRepo;
        this.bannerRepo = bannerRepo;
        this.artistProductRepo = artistProductRepo;
    }

    @Override
    public List<PartnerDto> getListPartner() {
        return partnerRepo.findAll().stream()
                .map(partner -> PartnerDto.builder().partner(partner).build())
                .collect(Collectors.toList());
    }

    @Override
    public List<BannerDto> getListBanner() {
        return bannerRepo.findAll().stream()
                .map(banner -> BannerDto.builder().banner(banner).build())
                .collect(Collectors.toList());
    }

    @Override
    public List<ProductHomeDto> getListProduct(Integer salientStatus) {
        Specification<ArtistProduct> specification = (root, cq, cb) -> cb.and(
                QueryUtils.buildEqFilter(root, cb, ArtistProduct_.SALIENT_STATUS, salientStatus)
        );
        List<ProductHomeDto> productHomeDtos = artistProductRepo.findAll(specification).stream()
                .map(artistProduct -> ProductHomeDto.builder().artistProduct(artistProduct).build())
                .collect(Collectors.toList());
        return productHomeDtos;
    }

}
