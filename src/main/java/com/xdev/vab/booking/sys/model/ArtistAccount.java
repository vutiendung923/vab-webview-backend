package com.xdev.vab.booking.sys.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.glassfish.jersey.internal.inject.Custom;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@ToString
@Entity
public class ArtistAccount extends BaseEntity {
    @Id
    private Integer id;
    private Integer artistId;
    private String fullName;
    private String avatar;
    private String account;
    private String password;
    private int status;
    private String refreshToken;
    private Date refreshTokenExpired;
    private String email;
    private String artistName;
}
