package com.xdev.vab.booking.sys.service.impl;

import com.xdev.vab.booking.common.Auth;
import com.xdev.vab.booking.common.PageData;
import com.xdev.vab.booking.sys.model.Artist;
import com.xdev.vab.booking.sys.model.ArtistBooking;
import com.xdev.vab.booking.sys.model.Customer;
import com.xdev.vab.booking.sys.repository.ArtistBookingRepo;
import com.xdev.vab.booking.sys.repository.ArtistRepo;
import com.xdev.vab.booking.sys.root.ArtistBooking_;
import com.xdev.vab.booking.sys.root.Artist_;
import com.xdev.vab.booking.sys.root.Customer_;
import com.xdev.vab.booking.sys.service.BookingService;
import com.xdev.vab.booking.sys.service.dto.booking.*;
import com.xdev.vab.booking.utils.QueryUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Join;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookingServiceImpl implements BookingService {

    private final ArtistBookingRepo artistBookingRepo;
    private final ArtistRepo artistRepo;

    public BookingServiceImpl(ArtistBookingRepo artistBookingRepo, ArtistRepo artistRepo) {
        this.artistBookingRepo = artistBookingRepo;
        this.artistRepo = artistRepo;
    }


    @Override
    @Transactional(rollbackOn = Exception.class)
    public ArtistBookingDtos setBooking(ArtistBookingDtos artistBookingDtos, Auth auth) {
        Integer artistId = artistBookingDtos.getArtistId();
        Artist artist = artistRepo.findById(artistId).orElseThrow(RuntimeException::new);
        ArtistBooking artistBooking = new ArtistBooking();
        artistBooking.setArtist(artist);
        if(auth != null){
            artistBooking.setCustomer(new Customer(auth.getId()));
            artistBooking.setCreateBy(auth.getUserName());
            artistBooking.setCreateTime(new Date());
        }
        artistBooking.setPhone(artistBookingDtos.getPhone());
        artistBooking.setFromTime(artistBookingDtos.getFromTime());
        artistBooking.setToTime(artistBookingDtos.getToTime());
        artistBooking.setExpectFee(artistBookingDtos.getExpectFee());
        artistBooking.setContent(artistBookingDtos.getContent());
        artistBooking.setAddress(artistBookingDtos.getAddress());
        artistBooking.setDescription(artistBookingDtos.getDescription());
        artistBooking.setStatus(0);
        return ArtistBookingDtos.builder().artistBookings(artistBookingRepo.save(artistBooking)).build();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public PageData<ArtistBookingDto> getBooked(Integer pageIndex, Integer pageSize, Integer status,
                                                String phone, String address,
                                                Integer custId, Integer artistId, String fromTime, String toTime,
                                                Integer performStatus) {
        Specification<ArtistBooking> specification = (root, cq, cb) -> {
            Join<ArtistBooking, Artist> artistBookingArtistJoin = root.join(ArtistBooking_.ARTIST);
            Join<ArtistBooking, Customer> artistBookingCustomerJoin = root.join(ArtistBooking_.CUSTOMER);
            return cb.and(
                    QueryUtils.buildEqFilter(root, cb, ArtistBooking_.STATUS, status),
                    QueryUtils.buildLikeFilter(root, cb, address, ArtistBooking_.ADDRESS),
                    QueryUtils.buildLikeFilter(root, cb, phone, ArtistBooking_.PHONE),
                    QueryUtils.buildGreaterThanFilter(root, cb, ArtistBooking_.FROM_TIME, fromTime, "dd/MM/yyyy"),
                    QueryUtils.buildLessThanFilter(root, cb, ArtistBooking_.TO_TIME, toTime, "dd/MM/yyyy"),
                    QueryUtils.buildEqFilter(root, cb, artistBookingArtistJoin.get(Artist_.ID), artistId),
                    QueryUtils.buildEqFilter(root, cb,artistBookingCustomerJoin.get(Customer_.ID), custId),
                    QueryUtils.buildEqFilter(root, cb, ArtistBooking_.PERFORM_STATUS, performStatus)
            );
        };
        long statusWait = artistBookingRepo.statusQuery0(custId, artistId);
        long statusAccept = artistBookingRepo.statusQuery1(custId, artistId);
        long statusDeny = artistBookingRepo.statusQuery2(custId, artistId);
        Page<ArtistBooking> artistBookingPage = artistBookingRepo.findAll(specification,
                PageRequest.of(pageIndex -1, pageSize, Sort.by(Sort.Direction.DESC, "createTime")));
        List<ArtistBookingDto> artistBookingDtoList = artistBookingPage.getContent()
                .stream()
                .map(artistBooking -> ArtistBookingDto.builder().artistBooking(artistBooking, statusWait, statusAccept, statusDeny).build())
                .collect(Collectors.toList());
        return PageData.of(artistBookingPage, artistBookingDtoList);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void setArtistVerify(ArtistBookingArtistVerifyDto artistBookingArtistVerifyDto, Auth auth) {
        Integer id = artistBookingArtistVerifyDto.getId();
        Integer artistId = artistBookingArtistVerifyDto.getArtistId();
        ArtistBooking artistBooking = artistBookingRepo.findArtistBookingsByIdAndArtistId(id, artistId);
        artistBooking.setStatus(artistBookingArtistVerifyDto.getStatus());
            if (artistBookingArtistVerifyDto.getStatus() == 1) {
                artistBooking.setPerformStatus(1);
            }
            if (artistBookingArtistVerifyDto.getStatus() == 0) {
                artistBooking.setPerformStatus(0);
            }
            if (artistBookingArtistVerifyDto.getStatus() == 2) {
                artistBooking.setPerformStatus(2);
            }
            if (auth != null) {
                artistBooking.setUpdateBy(auth.getUserName());
                artistBooking.setUpdateTime(new Date());
            }
            artistBookingRepo.save(artistBooking);
    }



    @Override
    @Transactional(rollbackOn = Exception.class)
    public void deleteRequest(Integer id) {
        ArtistBooking artistBooking = artistBookingRepo.findById(id).orElseThrow(RuntimeException::new);
        artistBookingRepo.delete(artistBooking);
    }

    @Override
    public void setPerformVerify(ArtistBookingPerformVerifyDto artistBookingPerformVerifyDto, Auth auth) {
        Integer id = artistBookingPerformVerifyDto.getId();
        Integer artistId = artistBookingPerformVerifyDto.getArtistId();
        ArtistBooking artistBooking = artistBookingRepo.findArtistBookingsByIdAndArtistId(id, artistId);
        artistBooking.setPerformStatus(artistBookingPerformVerifyDto.getPerformStatus());
        if(artistBookingPerformVerifyDto.getPerformStatus() == 0) {
            artistBooking.setStatus(0);
        }
        if(artistBookingPerformVerifyDto.getPerformStatus() == 1) {
            artistBooking.setStatus(1);
        }
        if(artistBookingPerformVerifyDto.getPerformStatus() == 2) {
            artistBooking.setStatus(2);
        }
        artistBookingRepo.save(artistBooking);
    }
}
