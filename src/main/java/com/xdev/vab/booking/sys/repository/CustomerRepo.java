package com.xdev.vab.booking.sys.repository;

import com.xdev.vab.booking.sys.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CustomerRepo extends JpaRepository<Customer, Integer> {
    @Query("select count(password) from Customer ")
    long check();
}
