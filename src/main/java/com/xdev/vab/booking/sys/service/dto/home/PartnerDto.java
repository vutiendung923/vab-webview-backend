package com.xdev.vab.booking.sys.service.dto.home;

import com.xdev.vab.booking.sys.model.Partner;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PartnerDto {
    private Integer id;
    private String name;
    private String image;
    private Integer numIndex;
    private Integer status;

    public static Builder builder() {
        return new  Builder();
    }

    public static class Builder{
        private Integer id;
        private String name;
        private String image;
        private Integer numIndex;
        private Integer status;

        public Builder partner(Partner partner) {
            this.id = partner.getId();
            this.name = partner.getName();
            this.image = partner.getImage();
            this.numIndex = partner.getNumIndex();
            this.status = partner.getStatus();
            return this;
        }

        public PartnerDto build() {
            return new PartnerDto(id, name, image, numIndex, status);
        }
    }
}
