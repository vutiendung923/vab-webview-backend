package com.xdev.vab.booking.sys.dao;

import com.xdev.vab.booking.sys.model.WebPage;
import com.xdev.vab.booking.sys.service.dto.WebPage.WebPageDto;

import java.util.List;

public interface ConfigDao {
    List<WebPageDto> getListWebPage(String requestId, Integer menuId, int langId);
}
