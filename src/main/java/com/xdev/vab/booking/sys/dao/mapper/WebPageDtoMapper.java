package com.xdev.vab.booking.sys.dao.mapper;

import com.xdev.vab.booking.sys.service.dto.WebPage.WebPageDto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class WebPageDtoMapper implements RowMapper<WebPageDto> {
    @Override
    public WebPageDto mapRow(ResultSet resultSet, int i) throws SQLException {
        WebPageDto dto = new WebPageDto();
        dto.setId(resultSet.getInt("ID"));
        dto.setName(resultSet.getString("NAME"));
        dto.setUrl(resultSet.getString("URL"));
        dto.setLangId(resultSet.getInt("LANG_ID"));
        dto.setPageType(resultSet.getInt("PAGE_TYPE"));
        return dto;
    }
}
