package com.xdev.vab.booking.sys.dao.mapper;

import com.xdev.vab.booking.sys.model.Artist;
import com.xdev.vab.booking.sys.model.ArtistLevel;
import com.xdev.vab.booking.sys.model.ArtistQualification;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ArtistMapper implements RowMapper<Artist> {
    @Override
    public Artist mapRow(ResultSet resultSet, int i) throws SQLException {
        Artist artist = new Artist();
        artist.setId(resultSet.getInt("ID"));
        artist.setEncodeId(resultSet.getString("ENCODE_ID"));
        artist.setCode(resultSet.getString("CODE"));
        artist.setArtisticName(resultSet.getString("ARTISTIC_NAME"));
        artist.setBirthday(resultSet.getDate("BIRTHDAY"));
        artist.setSex(resultSet.getInt("SEX"));
        artist.setAddress(resultSet.getString("ADDRESS"));
        artist.setPhone(resultSet.getString("PHONE"));
        artist.setNationality(resultSet.getInt("NATIONALITY"));
        artist.setEmail(resultSet.getString("EMAIL"));
        artist.setFbPage(resultSet.getString("FB_PAGE"));
        artist.setFbMessage(resultSet.getString("FB_MESSAGE"));
        artist.setInstagram(resultSet.getString("INSTAGRAM"));
        artist.setWhatsapp(resultSet.getString("WHATSAPP"));
        artist.setCreateTime(resultSet.getDate("CREATE_TIME"));
        artist.setCreateBy(resultSet.getString("CREATE_BY"));
        artist.setUpdateTime(resultSet.getDate("UPDATE_TIME"));
        artist.setUpdateBy(resultSet.getString("UPDATE_BY"));
        artist.setFullName(resultSet.getString("FULL_NAME"));
        artist.setWorkStatus(resultSet.getInt("WORK_STATUS"));
        artist.setShortDescription(resultSet.getString("SHORT_DESCRIPTION"));
        artist.setAvatar(resultSet.getString("AVATAR"));
        artist.setSocialEmbedded(resultSet.getString("SOCIAL_EMBEDDED"));
        artist.setFavoriteScore(resultSet.getInt("FAVORITE_SCORE"));
        artist.setViewArtist(resultSet.getInt("VIEW_ARTIST"));
        return artist;
    }
}
