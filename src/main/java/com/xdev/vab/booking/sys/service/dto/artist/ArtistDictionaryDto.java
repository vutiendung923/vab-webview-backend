package com.xdev.vab.booking.sys.service.dto.artist;

import com.xdev.vab.booking.sys.model.Artist;
import com.xdev.vab.booking.sys.model.ArtistLevel;
import com.xdev.vab.booking.sys.model.ArtistQualification;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ArtistDictionaryDto {
    private Integer artistId;
    private String levelName;
    private String qualificationName;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Integer artistId;
        private String levelName;
        private String qualificationName;

        public Builder artist(Artist artist) {
            this.artistId = artist.getId();
            this.levelName = artist.getArtistLevel().getName();
            this.qualificationName = artist.getArtistQualification().getName();
            return this;
        }

        public ArtistDictionaryDto build() {
            return new ArtistDictionaryDto(artistId, levelName, qualificationName);
        }
    }
}
