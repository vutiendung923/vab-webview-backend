package com.xdev.vab.booking.sys.controller;

import com.xdev.vab.booking.common.ResponseEntity;
import com.xdev.vab.booking.common.ResponseTypes;
import com.xdev.vab.booking.config.inject.CurrentRequestId;
import com.xdev.vab.booking.sys.model.PostCategory;
import com.xdev.vab.booking.sys.service.PostService;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Component
@Path("post")
@Valid
public class PostController {
    private final PostService postService;

    @CurrentRequestId
    private String requestId;

    public PostController(PostService postService) {
        this.postService = postService;
    }

    @GET
    public Response getList(@NotNull @QueryParam("langId") Integer langId,
                            @NotNull @QueryParam("pageIndex") Integer pageIndex,
                            @NotNull @QueryParam("pageSize") Integer pageSize,
                            @QueryParam("categoryId") Integer categoryId) {
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS, postService.getList(requestId, langId, pageIndex,
                pageSize, categoryId))).build();
    }

    @GET
    @Path("detail")
    public Response getDetail(@NotNull @QueryParam("id") Integer id,
                              @NotNull @QueryParam("langId") Integer langId) {
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS, postService.getDetail(requestId, langId, id))).build();
    }

    @GET
    @Path("category")
    public List<PostCategory> getListCategory(@NotNull @QueryParam("langId") Integer langId) {
        return postService.getListCategory(requestId, langId);
    }

}
