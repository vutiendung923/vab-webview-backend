package com.xdev.vab.booking.sys.service.dto.artist;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ArtistDetailDto {
    private ArtistProfile artistProfile;
    private ArtistDictionaryDto artistDictionaryDto;

}
