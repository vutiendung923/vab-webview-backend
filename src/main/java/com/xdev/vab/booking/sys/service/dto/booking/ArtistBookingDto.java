package com.xdev.vab.booking.sys.service.dto.booking;

import com.xdev.vab.booking.sys.model.ArtistBooking;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@AllArgsConstructor
public class ArtistBookingDto {
    private Integer id;
    private Integer customerId;
    private Integer artistId;
    private String artistName;
    private String artistFullName;
    private String avatar;
    private Date fromTime;
    private Date toTime;
    private String content;
    private String address;
    private String description;
    private Integer status;
    private long statusWait;
    private long statusAccept;
    private long statusDeny;
    private String createBy;
    private Date createTime;
    private String expectFee;
    private String phone;
    private Integer bookingCode;
    private String custName;
    private String mnName;
    private String mnPhone;
    private String mnFbmess;
    private String mnEmail;
    private Integer performStatus;

    public static Builder builder() {
        return new Builder();
    }


    public static class Builder {

        private Integer id;
        private Integer customerId;
        private Integer artistId;
        private String artistName;
        private String artistFullName;
        private String avatar;
        private Date fromTime;
        private Date toTime;
        private String content;
        private String address;
        private String description;
        private Integer status;
        private long statusWait;
        private long statusAccept;
        private long statusDeny;
        private Date createTime;
        private String createBy;
        private String expectFee;
        private String phone;
        private Integer bookingCode;
        private String custName;
        private String mnName;
        private String mnPhone;
        private String mnFbmess;
        private String mnEmail;
        private Integer performStatus;

        public Builder artistBooking(ArtistBooking artistBooking, long statusWait, long statusAccept, long statusDeny) {
            this.id = artistBooking.getId();
            if(artistBooking.getArtist() != null) {
                this.artistId = artistBooking.getArtist().getId();
                this.artistName = artistBooking.getArtist().getArtisticName();
                this.avatar = artistBooking.getArtist().getAvatar();
                this.artistFullName = artistBooking.getArtist().getFullName();
            }
            this.fromTime = artistBooking.getFromTime();
            this.toTime = artistBooking.getToTime();
            this.content = artistBooking.getContent();
            this.address = artistBooking.getAddress();
            this.description = artistBooking.getDescription();
            this.status = artistBooking.getStatus();
            this.statusWait = statusWait;
            this.statusAccept = statusAccept;
            this.statusDeny = statusDeny;
            this.customerId = artistBooking.getCustomer().getId();
            this.custName = artistBooking.getCustomer().getFullName();
            this.phone = artistBooking.getPhone();
            this.bookingCode = artistBooking.getBookingCode();
            this.expectFee = artistBooking.getExpectFee();
            this.createBy = artistBooking.getCreateBy();
            this.createTime = artistBooking.getCreateTime();
            if(artistBooking.getArtist().getMnName() == null &&  artistBooking.getArtist().getMnPhone() == null
                    && artistBooking.getArtist().getMnEmail() == null && artistBooking.getArtist().getMnFbmess() == null) {
                this.mnName = artistBooking.getArtist().getFullName();
                this.mnPhone = artistBooking.getArtist().getPhone();
                this.mnEmail = artistBooking.getArtist().getMnEmail();
                this.mnFbmess = artistBooking.getArtist().getFbMessage();
            } else {
                this.mnName = artistBooking.getArtist().getMnName();
                this.mnPhone = artistBooking.getArtist().getMnPhone();
                this.mnEmail = artistBooking.getArtist().getMnEmail();
                this.mnFbmess = artistBooking.getArtist().getMnFbmess();
            }
            if(artistBooking.getPerformStatus() != null) {
                this.performStatus = artistBooking.getPerformStatus();
            }
            return this;
        }

        public ArtistBookingDto build() {
            return new ArtistBookingDto(id, customerId, artistId, artistName, avatar, artistFullName, fromTime, toTime, content,
                                address, description, status,
                    statusWait, statusAccept, statusDeny, createBy, createTime, expectFee, phone, bookingCode, custName,
                    mnName, mnPhone, mnFbmess, mnEmail, performStatus);
        }
    }
}
