package com.xdev.vab.booking.sys.repository;

import com.xdev.vab.booking.sys.model.Partner;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PartnerRepo extends JpaRepository<Partner, Integer> {
}
