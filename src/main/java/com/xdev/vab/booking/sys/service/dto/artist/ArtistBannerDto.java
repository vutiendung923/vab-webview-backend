package com.xdev.vab.booking.sys.service.dto.artist;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@ToString
@Data
public class ArtistBannerDto {
    private int id;
    private int artistId;
    private String bannerUrl;
    private int status;
    private int numIndex;
    private Date createTime;
    private String createBy;
    private Date updateTime;
    private String updateBy;
}
