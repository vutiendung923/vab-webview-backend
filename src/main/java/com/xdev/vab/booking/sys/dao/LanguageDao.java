package com.xdev.vab.booking.sys.dao;

import com.xdev.vab.booking.sys.model.Language;

import java.util.List;

public interface LanguageDao {
    List<Language> getList(String requestId);
}
