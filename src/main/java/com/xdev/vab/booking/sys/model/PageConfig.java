package com.xdev.vab.booking.sys.model;
import com.xdev.vab.booking.common.Schema;
import lombok.Data;
import javax.persistence.*;

@Entity
@Data
@Table(name = "PAGE_CONFIG", schema = Schema.CMS)
public class PageConfig {
    @Id
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NAME_PAGE")
    private String namePage;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "CONTENT")
    private String content;

    @Column(name = "URL")
    private String url;

    @ManyToOne
    @JoinColumn(name = "PAGE_ID")
    private WebPage webPage;

}
