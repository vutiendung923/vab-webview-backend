package com.xdev.vab.booking.sys.service;

import com.xdev.vab.booking.common.Auth;
import com.xdev.vab.booking.sys.service.dto.account.LoginDto;
import com.xdev.vab.booking.sys.service.dto.account.LoginResponse;
import com.xdev.vab.booking.sys.service.dto.account.RegisterDto;

public interface AccountService {
    LoginResponse login(String requestId, LoginDto loginDto);

    void customerRegister(String requestId, RegisterDto registerDto);

    void customerChangePass(String requestId, LoginDto loginDto, Auth auth);

    void artistChangePass(String requestId, LoginDto loginDto, Auth auth);

    LoginResponse customerLogin(String requestId, LoginDto loginDto);

    String getAccessToken(String requestId, String refreshToken, String role);
}
