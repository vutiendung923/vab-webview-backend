package com.xdev.vab.booking.sys.service.impl;

import com.xdev.vab.booking.sys.dao.LanguageDao;
import com.xdev.vab.booking.sys.model.Language;
import com.xdev.vab.booking.sys.service.LanguageService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LanguageServiceImpl implements LanguageService {
    private final LanguageDao languageDao;

    public LanguageServiceImpl(LanguageDao languageDao) {
        this.languageDao = languageDao;
    }

    @Override
    public List<Language> getList(String requestId) {
        return languageDao.getList(requestId);
    }
}
