package com.xdev.vab.booking.sys.service.dto;

import com.xdev.vab.booking.sys.model.Product;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.*;


@Data
@AllArgsConstructor
public class ProductDto {
    private List<Integer> id;
    private Integer status;
    private Integer artistId;
}
