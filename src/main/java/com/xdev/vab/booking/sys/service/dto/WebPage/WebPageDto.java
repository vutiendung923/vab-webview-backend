package com.xdev.vab.booking.sys.service.dto.WebPage;

import com.xdev.vab.booking.sys.model.WebPage;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WebPageDto {
    private Integer id;
    private String name;
    private String url;
    private Integer status;
    private Integer langId;
    private Integer numIndex;
    private Integer pageType;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Integer id;
        private String name;
        private String url;
        private Integer status;
        private Integer langId;
        private Integer numIndex;
        private Integer pageType;

        public Builder webPage(WebPage webPage) {
            this.id = webPage.getId();
            this.name = webPage.getName();
            this.url = webPage.getUrl();
            this.status = webPage.getStatus();
            this.langId = webPage.getLangId();
            this.numIndex = webPage.getNumIndex();
            return this;
        }

        public WebPageDto build() {
            return new WebPageDto(id, name, url, status, langId, numIndex, pageType);
        }
    }
}
