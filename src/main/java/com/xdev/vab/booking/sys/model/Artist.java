package com.xdev.vab.booking.sys.model;

import com.xdev.vab.booking.common.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@ToString
@Entity
@Table(name = "ARTIST", schema = Schema.BOOKING)
public class Artist extends BaseEntity {
    @Id
    @Column(name = "ID")
    private Integer id;
    private String encodeId;
    private String code;
    private String artisticName;    // Nghệ danh
    private String avatar;  // ảnh đại diện
    private Date birthday;  // Ngày sinh
    private Integer sex;    // 1: nam, 2: nữ, 3: khác
    private String address; //Địa chỉ
    private String phone;   //Số điện thoại
    private Integer nationality;    //Quốc tịch
    private String email;   //Địa chỉ email
    private String fbPage;  //FB_PAGE
    private String fbMessage;
    private String instagram;
    private String whatsapp;
    private String fullName;    // Tên đầy đủ c nghệ sỹ
    private Integer workStatus;  //Trạng thái hoạt động
    private String shortDescription; //Mô tả ngắn ề nghệ sĩ
    private String socialEmbedded;
    private Integer favoriteScore;
    private Integer favoriteStatus;
    private String mnName;
    private String mnPhone;
    private String mnFbmess;
    private String mnEmail;
    private String mnIns;
    private String mnWhatsapp;

    @Column(name = "VIEW_ARTIST")
    private Integer viewArtist;

    @OneToMany(mappedBy = "artist")
    private List<ArtistBanner> artistBanners;

    @OneToMany(mappedBy = "artist")
    private List<ArtistBooking> artistBooking;


    @OneToMany(mappedBy = "artist")
    private List<ArtistStory> artistStorie;

    @OneToMany(mappedBy = "artist")
    private List<Product> product;

    @ManyToOne
    @JoinColumn(name = "ARTIST_LEVEL", referencedColumnName = "ID")
    private ArtistLevel artistLevel;

    @ManyToOne
    @JoinColumn(name = "QUALIFICATION", referencedColumnName = "ID")
    private ArtistQualification artistQualification;

    public Artist() {

    }

    public Artist(Integer id) {
        this.id = id;
    }
}
