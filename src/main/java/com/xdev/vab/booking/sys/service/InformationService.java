package com.xdev.vab.booking.sys.service;

import com.xdev.vab.booking.sys.service.dto.infor.InformationDto;

public interface InformationService {
    InformationDto getDetail(String requestId, Integer id);
}
