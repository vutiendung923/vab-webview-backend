package com.xdev.vab.booking.sys.repository;

import com.xdev.vab.booking.sys.model.PageConfig;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PageConfigRepo extends JpaRepository<PageConfig, Integer> {
}
