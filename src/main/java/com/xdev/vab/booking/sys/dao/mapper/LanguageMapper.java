package com.xdev.vab.booking.sys.dao.mapper;

import com.xdev.vab.booking.sys.model.Language;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LanguageMapper implements RowMapper<Language> {
    @Override
    public Language mapRow(ResultSet resultSet, int i) throws SQLException {
        Language language = new Language();
        language.setId(resultSet.getInt("ID"));
        language.setCode(resultSet.getString("CODE"));
        language.setName(resultSet.getString("NAME"));
        language.setIsDefault(resultSet.getInt("IS_DEFAULT"));
        language.setStatus(resultSet.getInt("STATUS"));
        return language;
    }
}
