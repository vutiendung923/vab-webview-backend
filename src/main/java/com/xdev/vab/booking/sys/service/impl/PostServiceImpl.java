package com.xdev.vab.booking.sys.service.impl;

import com.xdev.vab.booking.common.PageData;
import com.xdev.vab.booking.sys.dao.PostDao;
import com.xdev.vab.booking.sys.model.Post;
import com.xdev.vab.booking.sys.model.PostCategory;
import com.xdev.vab.booking.sys.service.PostService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImpl implements PostService {
    private final PostDao postDao;

    public PostServiceImpl(PostDao postDao) {
        this.postDao = postDao;
    }


    @Override
    public PageData<Post> getList(String requestId, Integer langId, Integer pageIndex, Integer pageSize, Integer categoryId) {
        return postDao.getList(requestId, langId, pageIndex, pageSize, categoryId);
    }

    @Override
    public Post getDetail(String requestId, Integer langId, Integer id) {
        return postDao.getDetail(requestId, langId, id);
    }

    @Override
    public List<PostCategory> getListCategory(String requestId, Integer langId) {
        return postDao.getListCategory(requestId, langId);
    }
}
