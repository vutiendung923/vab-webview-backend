package com.xdev.vab.booking.sys.dao.mapper;

import com.xdev.vab.booking.sys.service.dto.artist.ArtistDto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ArtistDtoMapper implements RowMapper<ArtistDto> {
    @Override
    public ArtistDto mapRow(ResultSet resultSet, int i) throws SQLException {
        ArtistDto artistDto = new ArtistDto();
        artistDto.setId(resultSet.getInt("ID"));
        artistDto.setEncodeId(resultSet.getString("ENCODE_ID"));
        artistDto.setCode(resultSet.getString("CODE"));
        artistDto.setArtistName(resultSet.getString("ARTISTIC_NAME"));
        artistDto.setAvatar(resultSet.getString("AVATAR"));
        artistDto.setFullName(resultSet.getString("FULL_NAME"));
        artistDto.setWorks(resultSet.getString("WORKS"));
        artistDto.setFavoriteStatus(resultSet.getInt("FAVORITE_STATUS"));
        return artistDto;
    }
}
