package com.xdev.vab.booking.sys.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@ToString
public class ArtistProduct extends BaseEntity {
    @Id
    private Integer id;
    private int artistId;
    private String code;
    private String name;
    private String embeddedUrl;
    private String imageUrl;
    private int status;
    private int numIndex;
    @Column(name = "SALIENT_STATUS")
    private Integer salientStatus;
}
