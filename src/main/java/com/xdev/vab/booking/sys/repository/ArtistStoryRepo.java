package com.xdev.vab.booking.sys.repository;

import com.xdev.vab.booking.sys.model.ArtistBanner;
import com.xdev.vab.booking.sys.model.ArtistStory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface ArtistStoryRepo extends JpaRepository<ArtistStory, Integer>, JpaSpecificationExecutor<ArtistStory> {
}
