package com.xdev.vab.booking.sys.controller;

import com.xdev.vab.booking.common.ResponseEntity;
import com.xdev.vab.booking.common.ResponseTypes;
import com.xdev.vab.booking.config.inject.CurrentRequestId;
import com.xdev.vab.booking.sys.service.HomeService;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.Date;


@Component
@Path("home")
@Valid
public class HomeController {

    private final HomeService homeService;

    @CurrentRequestId
    private String requestId;

    public HomeController(HomeService homeService) {
        this.homeService = homeService;
    }

    @Path("partner")
    @GET
    public Response getListPartner() {
        return Response.ok(new ResponseEntity( requestId,new Date(), ResponseTypes.SUCCESS,
                homeService.getListPartner())).build();
    }

    @Path("banner")
    @GET
    public Response getListBanner() {
        return Response.ok(new ResponseEntity(requestId,new Date(), ResponseTypes.SUCCESS,
                homeService.getListBanner())).build();
    }

    @GET
    @Path("product")
    public Response getListProduct(@QueryParam("salientStatus") Integer salientStatus) {
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS,
                homeService.getListProduct(salientStatus))).build();
    }

}
