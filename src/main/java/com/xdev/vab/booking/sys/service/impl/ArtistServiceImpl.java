package com.xdev.vab.booking.sys.service.impl;

import com.xdev.vab.booking.common.Auth;
import com.xdev.vab.booking.common.PageData;
import com.xdev.vab.booking.sys.dao.ArtistDao;
import com.xdev.vab.booking.sys.model.*;
import com.xdev.vab.booking.sys.repository.ArtistBannerRepo;
import com.xdev.vab.booking.sys.repository.ArtistRepo;
import com.xdev.vab.booking.sys.repository.ArtistStoryRepo;
import com.xdev.vab.booking.sys.repository.ProductRepo;
import com.xdev.vab.booking.sys.root.Artist_;
import com.xdev.vab.booking.sys.service.ArtistService;
import com.xdev.vab.booking.sys.service.dto.ProductDto;
import com.xdev.vab.booking.sys.service.dto.artist.*;
import com.xdev.vab.booking.utils.QueryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class ArtistServiceImpl implements ArtistService {
    private final ArtistDao artistDao;
    private final ArtistBannerRepo artistBannerRepo;
    private final ArtistRepo artistRepo;
    private final ArtistStoryRepo artistStoryRepo;
    private final ProductRepo productRepo;

    @Autowired
    public ArtistServiceImpl(ArtistDao artistDao,
                             ArtistBannerRepo artistBannerRepo,
                             ArtistRepo artistRepo, ArtistStoryRepo artistStoryRepo, ProductRepo productRepo) {
        this.artistDao = artistDao;
        this.artistBannerRepo = artistBannerRepo;
        this.artistRepo = artistRepo;
        this.artistStoryRepo = artistStoryRepo;
        this.productRepo = productRepo;
    }

    @Override
    public PageData<ArtistDto> getList(String requestId, Integer pageIndex, Integer pageSize, Auth auth, String artisticName) {
        PageData<ArtistDto> pageData = artistDao.getList(requestId, pageIndex, pageSize, auth, artisticName);
        pageData.getData().forEach(artistDto -> artistDto.setRank(new Random().nextInt(9) + 1));
        return pageData;
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public ArtistProfile getProfile(String requestId, Auth auth, int artistId, ArtistDto artistDto) {
        ArtistProfile artistProfile = artistDao.getProfile(requestId, artistId, auth);
        Artist artist = artistRepo.findById(artistId).orElseThrow(RuntimeException::new);
        artist.setViewArtist(artist.getViewArtist()+1);
        artistRepo.save(artist);
        return artistProfile;
    }

    @Override
    public ArtistDictionaryDto getDictionary(Integer artistId) {
        return ArtistDictionaryDto.builder().artist(artistRepo.findById(artistId).orElseThrow(RuntimeException::new)).build();
    }

    @Override
    public void updateSocialEmbedded(String requestId, ArtistSocialEmbeddedDto artistSocialEmbeddedDto) {
        artistDao.updateSocialEmbedded(requestId, artistSocialEmbeddedDto);
    }

    @Override
    public void updateStory(String requestId, ArtistStoryDto artistStoryDto, Auth auth) {
        Integer id = artistStoryDto.getId();
        ArtistStory artistStory = artistStoryRepo.findById(id)
                .orElseThrow(RuntimeException::new);
        artistStory.setImageUrl(artistStoryDto.getImageUrl());
        artistStory.setTitle(artistStoryDto.getTitle());
        artistStory.setContent(artistStoryDto.getContent());
        artistStory.setStatus(artistStory.getStatus());
        artistStory.setCreateTime(new Date());
        if(auth != null) {
            artistStory.setUpdateBy(auth.getUserName());
            artistStory.setUpdateTime(new Date());
        }
        artistStoryRepo.save(artistStory);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void addStory(String requestId, ArtistStoryDto artistStoryDto, Auth auth) {
        Integer artistId = artistStoryDto.getArtistId();
        Artist artist = artistRepo.findById(artistId).orElseThrow(RuntimeException::new);
        ArtistStory artistStory = new ArtistStory();
        artistStory.setArtist(artist);
        artistStory.setImageUrl(artistStoryDto.getImageUrl());
        artistStory.setTitle(artistStoryDto.getTitle());
        artistStory.setContent(artistStoryDto.getContent());
        artistStory.setStatus(artistStory.getStatus());
        artistStory.setCreateTime(new Date());
        if(auth != null) {
            artistStory.setCreateBy(auth.getUserName());
            artistStory.setCreateTime(new Date());
        }
        artistStoryRepo.save(artistStory);
    }

    @Override
    public void deleteStory(String requestId, Integer id) {
        ArtistStory artistStory = artistStoryRepo.findById(id).orElseThrow(RuntimeException::new);
        artistStoryRepo.delete(artistStory);
    }


    @Override
    public void addProduct(String requestId, ArtistProductDto artistProductDto) {
        artistDao.addProduct(requestId, artistProductDto);
    }

    @Override
    public void updateProduct(String requestId, ArtistProductDto productDto) {
        artistDao.updateProduct(requestId, productDto);
    }

    @Override
    public void deleteProduct(String requestId, int artistId, int productId) {
        artistDao.deleteProduct(requestId, artistId, productId);
    }

    @Override
    public void favorite(String requestId, Integer artistId, Auth auth) {
        artistDao.favorite(requestId, artistId, auth);
    }

    @Override
    public void unFavorite(String requestId, Integer artistId, Auth auth) {
        artistDao.unFavorite(requestId, artistId, auth);
    }


    @Override
    @Transactional(rollbackOn = Exception.class)
    public void addBanner(String requestId, ArtistBannerDto artistBannerDto, Auth auth) {
        Integer artistId = artistBannerDto.getArtistId();
        Artist artist = artistRepo.findById(artistId).orElseThrow(RuntimeException::new);
        ArtistBanner artistBanner = new ArtistBanner();
        artistBanner.setArtist(artist);
        artistBanner.setBannerUrl(artistBannerDto.getBannerUrl());
        artistBanner.setCreateTime(new Date());
        if(auth != null) {
            artistBanner.setCreateBy(auth.getUserName());
        }
        if (artistBanner.getNumIndex() == null) {
            artistBanner.setNumIndex(3);
            artistBannerRepo.updateNumIndex();
        }
        artistBanner.setStatus(artistBannerDto.getStatus());
        artistBannerRepo.save(artistBanner);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void updateBanner(String requestId, ArtistBannerDto artistBannerDto, Auth auth) {
        int id = artistBannerDto.getId();
        ArtistBanner artistBanner = artistBannerRepo.findById(id)
                .orElseThrow(RuntimeException::new);
        artistBanner.setBannerUrl(artistBannerDto.getBannerUrl());
        artistBanner.setUpdateTime(new Date());
        if(auth != null) {
            artistBanner.setUpdateBy(auth.getUserName());
        }
        if (artistBanner.getNumIndex() == null) {
            artistBanner.setNumIndex(3);
            artistBannerRepo.updateNumIndex();
        }
        artistBanner.setStatus(artistBannerDto.getStatus());
        artistBannerRepo.save(artistBanner);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void delete(String requestId, Integer id) {
        ArtistBanner artistBanner = artistBannerRepo.findById(id)
                .orElseThrow(RuntimeException::new);
        artistBannerRepo.delete(artistBanner);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void pushProduct(String requestId, ProductDto productDto) {
        Integer artistId = productDto.getArtistId();
        productRepo.removeProductHighlight(artistId);

        List<Product> products = productDto.getId().stream().map(productId -> {
            return new Product(productId, new Artist(artistId), productDto.getStatus());
                }
                ).collect(Collectors.toList());

        productRepo.saveAll(products);
    }

    @Override
    public PageData<ArtistDto> getName(Integer pageIndex, Integer pageSize, String artisticName) {
        Specification<Artist> specification = (root, cq, cb) -> cb.and(
                QueryUtils.buildLikeFilter(root, cb, artisticName, Artist_.ARTISTIC_NAME)
        );
        Page<Artist> artistPage = artistRepo.findAll(specification, PageRequest.of(pageIndex-1, pageSize));
        List<ArtistDto> artistDto = artistPage.getContent()
                .stream()
                .map(ArtistDto::new)
                .collect(Collectors.toList());
        return PageData.of(artistPage, artistDto);
    }

    @Override
    public void updateDescription(ArtistDescriptionDto artistDescriptionDto) {
        Integer id = artistDescriptionDto.getId();
        Artist artist = artistRepo.findById(id).orElseThrow(RuntimeException::new);
        artist.setArtisticName(artistDescriptionDto.getArtisticName());
        artist.setShortDescription(artistDescriptionDto.getDescriptionShort());
        artistRepo.save(artist);
    }

}
