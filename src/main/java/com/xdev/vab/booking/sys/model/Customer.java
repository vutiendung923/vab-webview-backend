package com.xdev.vab.booking.sys.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;


@Entity
@Data
@ToString
public class Customer {
    @Id
    private Integer id;

    private String code;
    private String userName;
    private String fullName;
    private String companyName;
    private String taxCode;
    private String email;
    private Integer customerLevel;
    private Integer status;
    private String phone;
    private String password;
    private Date createTime;
    private String createBy;
    private Date updateTime;
    private String updateBy;
    private String refreshToken;
    private Date refreshTokenExpired;
    private String address;
    private String type;
    private String representative;
    private String position;

    @OneToMany(mappedBy = "customer")
    private List<ArtistBooking> artistBooking;

    public Customer() {

    }

    public Customer(Integer id) {
        this.id = id;
    }
}
