package com.xdev.vab.booking.sys.model;

import com.xdev.vab.booking.common.Schema;
import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Data
@ToString
@Table(name = "WEB_PAGE", schema = Schema.CMS)
public class WebPage {
    @Id
    private int id;
    private String name;
    private String url;
    private int status;
    private int langId;
    private int numIndex;

    @OneToMany(mappedBy = "webPage")
    private List<PageConfig> pageConfig;
}
