package com.xdev.vab.booking.sys.root;


public class ArtistBooking_ {
    public static final String FROM_TIME = "fromTime";
    public static final String TO_TIME = "toTime";
    public static final String STATUS = "status";
    public static final String PHONE = "phone";
    public static final String ADDRESS = "address";
    public static final String ARTIST = "artist";
    public static final String CUSTOMER = "customer";
    public static final String PERFORM_STATUS = "performStatus";
}
