package com.xdev.vab.booking.sys.dao.impl;

import com.xdev.vab.booking.sys.dao.LanguageDao;
import com.xdev.vab.booking.sys.dao.mapper.LanguageMapper;
import com.xdev.vab.booking.sys.model.Language;
import com.xdev.vab.booking.sys.service.impl.StoreProcedureCaller;
import oracle.jdbc.OracleTypes;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

@Repository
public class LanguageDaoImpl implements LanguageDao {
    private final DataSource dataSource;

    public LanguageDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Language> getList(String requestId) {
        StoreProcedureCaller caller = new StoreProcedureCaller(dataSource, requestId, "PKG_COMMON", "GET_LIST_LANGUAGE");
        Map<String, Object> result = caller
                .register("P_CODE", OracleTypes.INTEGER)
                .register("P_DETAIL", OracleTypes.VARCHAR)
                .register("P_LIST_LANGUAGE", OracleTypes.CURSOR)
                .returningResultSet("P_LIST_LANGUAGE", new LanguageMapper())
                .executeOnError();

        return (List<Language>) result.get("P_LIST_LANGUAGE");
    }
}
