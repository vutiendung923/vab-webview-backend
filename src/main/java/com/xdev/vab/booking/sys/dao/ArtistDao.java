package com.xdev.vab.booking.sys.dao;

import com.xdev.vab.booking.common.Auth;
import com.xdev.vab.booking.common.PageData;
import com.xdev.vab.booking.sys.service.dto.artist.*;

public interface ArtistDao {
    PageData<ArtistDto> getList(String requestId, Integer pageIndex, Integer pageSize, Auth auth, String artisticName);

    ArtistProfile getProfile(String requestId, int artistId, Auth auth);

    void updateSocialEmbedded(String requestId, ArtistSocialEmbeddedDto artistSocialEmbeddedDto);

    void addProduct(String requestId, ArtistProductDto productDto);

    void updateProduct(String requestId, ArtistProductDto productDto);

    void deleteProduct(String requestId, int artistId, int productId);

    void favorite(String requestId, int artistId, Auth auth);

    void unFavorite(String requestId, int artistId, Auth auth);
}
