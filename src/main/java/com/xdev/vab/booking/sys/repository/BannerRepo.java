package com.xdev.vab.booking.sys.repository;

import com.xdev.vab.booking.sys.model.Banner;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BannerRepo extends JpaRepository<Banner, Integer> {
}
