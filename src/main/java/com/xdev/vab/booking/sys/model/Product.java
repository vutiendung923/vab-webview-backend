package com.xdev.vab.booking.sys.model;

import com.xdev.vab.booking.common.Schema;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "ARTIST_PRODUCT", schema = Schema.BOOKING)
public class Product {
    @Id
    @Column(name = "ID")
    private Integer id;

    @Column(name = "STATUS")
    Integer status;

    @ManyToOne
    @JoinColumn(name = "ARTIST_ID")
    private Artist artist;


    public Product(Integer id) {
        this.id = id;
    }

    public Product(Integer id, Artist artist, Integer status) {
        this.id = id;
        this.artist = artist;
        this.status = status;
    }

    public Product() {
    }

    public void toProduct(Integer id, Artist artist){
        this.id = id;
        this.artist = artist;
    }
}
