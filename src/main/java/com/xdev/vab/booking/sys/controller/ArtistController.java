package com.xdev.vab.booking.sys.controller;

import com.xdev.vab.booking.common.Auth;
import com.xdev.vab.booking.common.ResponseEntity;
import com.xdev.vab.booking.common.ResponseTypes;
import com.xdev.vab.booking.common.Roles;
import com.xdev.vab.booking.config.filter.OneTPS;
import com.xdev.vab.booking.config.inject.CurrentAuth;
import com.xdev.vab.booking.config.inject.CurrentRequestId;
import com.xdev.vab.booking.sys.service.ArtistService;
import com.xdev.vab.booking.sys.service.dto.ProductDto;
import com.xdev.vab.booking.sys.service.dto.artist.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.relational.core.sql.In;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Component
@Path("artist")
@Valid
@Slf4j
public class ArtistController {
    private final ArtistService artistService;

    @Autowired
    public ArtistController(ArtistService artistService) {
        this.artistService = artistService;
    }

    @CurrentRequestId
    private String requestId;

    @CurrentAuth
    private Auth auth;

    @GET
    public Response getList(@NotNull @QueryParam("pageIndex") Integer pageIndex,
                            @NotNull @QueryParam("pageSize") Integer pageSize,
                            @QueryParam("artisticName") String artisticName) {
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS, artistService.getList(requestId, pageIndex, pageSize, auth, artisticName))).build();
    }

    @GET
    @Path("profile")
    @PermitAll
    public Response getProfile(ArtistDto artistDto,@NotNull @QueryParam("artistId") Integer artistId) {
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS, artistService.getProfile(requestId, auth, artistId, artistDto))).build();
    }

    @GET
    @Path("dictionary")
    public Response getDictionary(@NotNull @QueryParam("artistId") Integer artistId) {
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS,
                artistService.getDictionary(artistId))).build();
    }

    @POST
    @Path("socialEmbedded/update")
    @RolesAllowed(Roles.ARTIST)
    public Response updateSocialEmbedded(@NotNull ArtistSocialEmbeddedDto artistSocialEmbeddedDto) {
        artistSocialEmbeddedDto.setArtistId(auth.getId());
        artistService.updateSocialEmbedded(requestId, artistSocialEmbeddedDto);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @POST
    @Path("story/add")
    @RolesAllowed(Roles.ARTIST)
    public Response addStory(@NotNull ArtistStoryDto artistStoryDto) {
        artistService.addStory(requestId, artistStoryDto, auth);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @POST
    @Path("story/update")
    @RolesAllowed(Roles.ARTIST)
    public Response updateStory(@NotNull ArtistStoryDto artistStoryDto) {
        artistService.updateStory(requestId, artistStoryDto, auth);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @GET
    @Path("story/delete")
    @RolesAllowed(Roles.ARTIST)
    public Response deleteStory(@QueryParam("id") Integer id){
        artistService.deleteStory(requestId, id);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @POST
    @Path("product/push")
    @RolesAllowed(Roles.ARTIST)
    public Response pushProduct(ProductDto productDto) {
        artistService.pushProduct(requestId,  productDto);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @POST
    @Path("product/add")
    @RolesAllowed(Roles.ARTIST)
    public Response addProduct(@NotNull ArtistProductDto productDto) {
        productDto.setArtistId(auth.getId());
        artistService.addProduct(requestId, productDto);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @POST
    @Path("product/update")
    @RolesAllowed(Roles.ARTIST)
    public Response updateProduct(@NotNull ArtistProductDto productDto) {
        productDto.setArtistId(auth.getId());
        artistService.updateProduct(requestId, productDto);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @GET
    @Path("product/delete")
    @RolesAllowed(Roles.ARTIST)
    public Response deleteProduct(@NotNull @QueryParam("productId") Integer productId) {
        artistService.deleteProduct(requestId, auth.getId(), productId);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @GET
    @Path("favorite")
    @OneTPS
    @RolesAllowed({Roles.ARTIST, Roles.CUSTOMER, Roles.VIEWER})
    public Response favorite(@NotNull @QueryParam("artistId") Integer id, @Context HttpServletRequest httpServletRequest) {
        artistService.favorite(requestId, id, auth);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @GET
    @Path("unFavorite")
    @RolesAllowed({Roles.ARTIST, Roles.CUSTOMER, Roles.VIEWER})
    public Response unFavorite(@NotNull @QueryParam("artistId") Integer artistId) {
        artistService.unFavorite(requestId, artistId, auth);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @POST
    @Path("banner/add")
    @RolesAllowed({Roles.ARTIST})
    public Response addBanner(@NotNull ArtistBannerDto artistBannerDto) {
        artistService.addBanner(requestId, artistBannerDto, auth);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @POST
    @Path("banner/update")
    @RolesAllowed({Roles.ARTIST})
    public Response updateBanner(@NotNull ArtistBannerDto artistBannerDto) {
        artistService.updateBanner(requestId, artistBannerDto, auth);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @GET
    @Path("banner/delete")
    @RolesAllowed({Roles.ARTIST})
    public Response deleteBanner(@NotNull @QueryParam("id") Integer id) {
        artistService.delete(requestId, id);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }

    @POST
    @Path("description/update")
    @RolesAllowed(Roles.ARTIST)
    public Response updateDescription(ArtistDescriptionDto artistDescriptionDto) {
        artistService.updateDescription(artistDescriptionDto);
        return Response.ok(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS)).build();
    }
}
