package com.xdev.vab.booking.sys.repository;

import com.xdev.vab.booking.sys.model.Information;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InformationRepo extends JpaRepository<Information, Integer> {
}
