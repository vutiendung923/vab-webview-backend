package com.xdev.vab.booking.sys.service;

import com.xdev.vab.booking.sys.service.dto.WebPage.PageConfigDetailDto;
import com.xdev.vab.booking.sys.service.dto.WebPage.WebPageDto;
import com.xdev.vab.booking.sys.service.dto.home.ProductHomeDto;

import java.util.List;

public interface WebPageService {
    List<WebPageDto> getListWebPage(String requestId, Integer menuId, Integer langId);

    List<PageConfigDetailDto> getDetail(String requestId, Integer pageId, String path, Integer langId);

}
