package com.xdev.vab.booking.sys.service.dto.home;

import com.xdev.vab.booking.sys.model.Banner;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BannerDto {
    private Integer id;
    private String name;
    private Integer numIndex;
    private String image;
    private Integer status;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder{
        private Integer id;
        private String name;
        private Integer numIndex;
        private String image;
        private Integer status;

        public Builder banner(Banner banner) {
            this.id = banner.getId();
            this.name = banner.getName();
            this.numIndex = banner.getNumIndex();
            this.image = banner.getImage();
            this.status = banner.getStatus();
            return this;
        }

        public BannerDto build() {
            return new BannerDto(id, name, numIndex, image, status);
        }
    }
}
