package com.xdev.vab.booking.config.inject.impl;

import com.xdev.vab.booking.common.Constant;
import com.xdev.vab.booking.config.inject.CurrentRequestId;
import org.glassfish.hk2.api.Injectee;
import org.glassfish.hk2.api.InjectionResolver;
import org.glassfish.hk2.api.ServiceHandle;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.ext.Provider;

@Provider
public class CurrentRequestIdInjectionResolve implements InjectionResolver<CurrentRequestId> {
    @Inject
    private javax.inject.Provider<ContainerRequestContext> requestContext;

    @Override
    public Object resolve(Injectee injectee, ServiceHandle<?> serviceHandle) {
        return requestContext.get().getProperty(Constant.TRACING_LOG_ATTRIBUTE_NAME);
    }

    @Override
    public boolean isConstructorParameterIndicator() {
        return false;
    }

    @Override
    public boolean isMethodParameterIndicator() {
        return false;
    }
}
