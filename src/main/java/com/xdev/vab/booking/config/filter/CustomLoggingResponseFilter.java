package com.xdev.vab.booking.config.filter;

import com.xdev.vab.booking.common.ResponseEntity;
import com.xdev.vab.booking.common.ResponseTypes;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.logging.LoggingFeature;

import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.Date;

import static com.xdev.vab.booking.common.Constant.MONITOR_LOG_ATTRIBUTE_NAME;
import static com.xdev.vab.booking.common.Constant.TRACING_LOG_ATTRIBUTE_NAME;


@Provider
@Priority(1)
@PreMatching
public class CustomLoggingResponseFilter extends LoggingFeature implements ContainerResponseFilter {

    private static final Log log = LogFactory.getLog(CustomLoggingResponseFilter.class);

    @Context
    private HttpServletRequest httpServletRequest;

    @Context
    private HttpServletResponse httpServletResponse;

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        String requestId = (String) httpServletRequest.getAttribute(TRACING_LOG_ATTRIBUTE_NAME);

        if (responseContext != null) {
            responseContext.getHeaders().putSingle("Access-Control-Allow-Credentials", "true");
            responseContext.getHeaders().putSingle("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
            responseContext.getHeaders().putSingle("Access-Control-Allow-Headers", "*");
            responseContext.getHeaders().putSingle("Content-Type", "application/json; charset=utf-8");
            responseContext.getHeaders().putSingle("Access-Control-Allow-Origin", "*");

            Object responseEntity = responseContext.getEntity();
            if (responseEntity == null) {
                if (responseContext.getStatus() == Response.Status.NO_CONTENT.getStatusCode()) {
                    responseContext.setStatus(Response.Status.OK.getStatusCode());
                }
                responseContext.setEntity(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS, new Object()));
            } else {
                if (!(responseEntity instanceof ResponseEntity)) {
                    responseContext.setEntity(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS, responseEntity));
                }
            }
        }

        StringBuilder sb = new StringBuilder();
        sb.append("RequestId: ").append(requestId);
        sb.append(" - Header: ").append(responseContext == null ? null : responseContext.getHeaders());
        sb.append(" - Entity: ").append(responseContext == null ? null : responseContext.getEntity());
        sb.append(" - In: ").append(System.currentTimeMillis() - (long) httpServletRequest.getAttribute(MONITOR_LOG_ATTRIBUTE_NAME)).append(" ms");
        log.info("HTTP RESPONSE ==> " + sb.toString());
    }
}
