package com.xdev.vab.booking.config.filter;

import com.xdev.vab.booking.common.Constant;
import com.xdev.vab.booking.common.ResponseEntity;
import com.xdev.vab.booking.common.ResponseTypes;
import com.xdev.vab.booking.sys.service.SpamService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Date;

@OneTPS
@Priority(2)
@Component
@Slf4j
public class OneTpsImpl implements ContainerRequestFilter {
    private final SpamService spamService;

    @Context
    private HttpServletRequest httpServletRequest;

    @Autowired
    public OneTpsImpl(SpamService spamService) {
        this.spamService = spamService;
    }

    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        String requestId = (String) httpServletRequest.getAttribute(Constant.TRACING_LOG_ATTRIBUTE_NAME);

        if (spamService.spamPerSecond(httpServletRequest.getSession())) {
            log.info(String.format("requestId: %s, Exceed TPS !!!", requestId));
            containerRequestContext
                    .abortWith(Response.status(Response.Status.OK)
                            .entity(new ResponseEntity(requestId, new Date(), ResponseTypes.SUCCESS.getCode(), "Exceed TPS !"))
                            .build());
        }
    }
}
