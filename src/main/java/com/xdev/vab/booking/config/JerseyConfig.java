package com.xdev.vab.booking.config;

import com.xdev.vab.booking.config.filter.*;
import com.xdev.vab.booking.exception.mapper.ConstraintViolationExceptionMapper;
import com.xdev.vab.booking.exception.mapper.DaoExceptionMapper;
import com.xdev.vab.booking.exception.mapper.GenericExceptionMapper;
import com.xdev.vab.booking.sys.controller.*;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.springframework.stereotype.Component;

@Component
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        register(ArtistController.class);
        register(AccountController.class);
        register(LanguageController.class);
        register(PostController.class);
        register(BookingController.class);
        register(InforController.class);
        register(HomeController.class);

        register(AuthFilter.class);
        register(ClientLoggingFilter.class);
        register(CustomLoggingRequestFilter.class);
        register(CustomLoggingResponseFilter.class);
        register(IpFilterImpl.class);
        register(OneTpsImpl.class);
        register(WebPageController.class);

        register(ConstraintViolationExceptionMapper.class);
        register(DaoExceptionMapper.class);
        register(GenericExceptionMapper.class);

        register(RolesAllowedDynamicFeature.class);
        property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
        property(ServerProperties.BV_DISABLE_VALIDATE_ON_EXECUTABLE_OVERRIDE_CHECK, true);
        register(com.xdev.vab.booking.config.BeanConfig.class);
        register(JsonConfig.class);
    }
}
