package com.xdev.vab.booking.config.filter;

import com.xdev.vab.booking.common.Constant;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Provider
@IpFilter
@Priority(1)
public class IpFilterImpl implements ContainerRequestFilter {
    private final Log log = LogFactory.getLog(this.getClass());
    private static final List<String> whiteList = Collections.synchronizedList(new ArrayList<>());

    static {
        whiteList.add("127.0.0.1");
        whiteList.add("0:0:0:0:0:0:0:1");
    }

    @Context
    private HttpServletRequest httpServletRequest;

    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        String requestId = (String) httpServletRequest.getAttribute(Constant.TRACING_LOG_ATTRIBUTE_NAME);
        String remoteIp = getRemoteIp();
        if (!whiteList.contains(remoteIp)) {
            log.info(String.format("requestId: %s, Remote Ip %s can not access resource !", requestId, remoteIp));
            containerRequestContext.abortWith(Response.status(Response.Status.FORBIDDEN).entity("Your IP can not access resource !").build());
            return;
        }

        log.debug(String.format("requestId: %s, Remote Ip %s was accepted !", requestId, remoteIp));
    }

    private String getRemoteIp() {
        String remoteIp = httpServletRequest.getHeader("X-FORWARDED-FOR");
        if (StringUtils.isBlank(remoteIp)) {
            return httpServletRequest.getRemoteAddr();
        }
        return remoteIp;
    }

}
