package com.xdev.vab.booking.config.inject.impl;

import com.xdev.vab.booking.common.Auth;
import com.xdev.vab.booking.config.inject.CurrentAuth;
import org.glassfish.hk2.api.Injectee;
import org.glassfish.hk2.api.InjectionResolver;
import org.glassfish.hk2.api.ServiceHandle;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.ext.Provider;

@Provider
public class CurrentUserInjectionResolver implements InjectionResolver<CurrentAuth> {

    @Inject
    private javax.inject.Provider<ContainerRequestContext> requestContext;

    @Override
    public Object resolve(Injectee injectee, ServiceHandle<?> sh) {
        if (Auth.class == injectee.getRequiredType()) {
            return requestContext.get().getProperty("user");
        }
        return null;
    }

    @Override
    public boolean isConstructorParameterIndicator() {
        return false;
    }

    @Override
    public boolean isMethodParameterIndicator() {
        return false;
    }
}
