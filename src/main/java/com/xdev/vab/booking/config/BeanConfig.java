package com.xdev.vab.booking.config;

import com.xdev.vab.booking.config.inject.CurrentAuth;
import com.xdev.vab.booking.config.inject.CurrentIp;
import com.xdev.vab.booking.config.inject.CurrentRequestId;
import com.xdev.vab.booking.config.inject.impl.CurrentIpInjectionResolve;
import com.xdev.vab.booking.config.inject.impl.CurrentRequestIdInjectionResolve;
import com.xdev.vab.booking.config.inject.impl.CurrentUserInjectionResolver;
import com.xdev.vab.booking.sys.service.JwtService;
import com.xdev.vab.booking.sys.service.impl.JwtServiceImpl;
import org.glassfish.hk2.api.InjectionResolver;
import org.glassfish.hk2.api.TypeLiteral;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

import javax.inject.Singleton;
import javax.ws.rs.ext.Provider;

@Provider
public class BeanConfig extends AbstractBinder {
    @Override
    protected void configure() {
        bind(CurrentUserInjectionResolver.class).to(new TypeLiteral<InjectionResolver<CurrentAuth>>() {
        }).in(Singleton.class);
        bind(CurrentRequestIdInjectionResolve.class).to(new TypeLiteral<InjectionResolver<CurrentRequestId>>() {
        }).in(Singleton.class);
        bind(CurrentIpInjectionResolve.class).to(new TypeLiteral<InjectionResolver<CurrentIp>>() {
        }).in(Singleton.class);
        bind(JwtServiceImpl.class).to(JwtService.class).in(Singleton.class);
    }
}
