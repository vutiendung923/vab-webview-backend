package com.xdev.vab.booking.config.validation.impl;


import com.xdev.vab.booking.config.validation.DatePattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DatePatternValidator implements ConstraintValidator<DatePattern, String> {
    private String pattern;

    @Override
    public void initialize(DatePattern datePattern) {
        this.pattern = datePattern.pattern();
    }

    @Override
    public boolean isValid(String dateTime, ConstraintValidatorContext constraintValidatorContext) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        try {
            simpleDateFormat.parse(dateTime);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }
}
