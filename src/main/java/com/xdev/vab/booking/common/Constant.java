package com.xdev.vab.booking.common;

public class Constant {
    public static final String AUTH_INFO_ATTRIBUTE_NAME = "auth-info";
    public static final String TRACING_LOG_ATTRIBUTE_NAME = "x-request-id-x";
    public static final String MONITOR_LOG_ATTRIBUTE_NAME = "x-begin-at-x";
    public static final String TRACING_IP_ATTRIBUTE_NAME = "x-current-ip-x";
    public static final String DATETIME_FORMAT = "dd/MM/yyyy HH:mm:ss";
    public static final String DATE_FORMAT = "dd/MM/yyyy";
    public static boolean DEBUG_MODE = false;
    public static final String CHANNEL = "GAME_LAUNCHER";
    public static final String DEFAULT_PASSWORD = "123@123!";
}
