package com.xdev.vab.booking.common;

import com.google.gson.Gson;
import lombok.Data;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.Date;


@Data
public class ResponseEntity {
    private String requestId;
    private String at;
    private ResponseError error;
    private Object data;

    public ResponseEntity(String requestId, Date at, ResponseError error, Object data) {
        this.requestId = requestId;
        this.at = DateFormatUtils.format(at, Constant.DATETIME_FORMAT);
        this.error = error;
        this.data = data;
    }

    public ResponseEntity(String requestId, Date at, ResponseError error, Object data, Exception ex) {
        this.requestId = requestId;
        this.at = DateFormatUtils.format(at, Constant.DATETIME_FORMAT);
        this.error = error;
        this.data = data;
    }

    public ResponseEntity(String requestId, Date at, ResponseError error) {
        this.requestId = requestId;
        this.at = DateFormatUtils.format(at, Constant.DATETIME_FORMAT);
        this.error = error;
    }

    public ResponseEntity(String requestId, Date at, Object data) {
        this.requestId = requestId;
        this.at = DateFormatUtils.format(new Date(), Constant.DATETIME_FORMAT);
        this.error = ResponseTypes.SUCCESS;
        this.data = data;
    }

    public ResponseEntity(String requestId, Date at, ResponseError error, Exception ex) {
        this.requestId = requestId;
        this.at = DateFormatUtils.format(at, Constant.DATETIME_FORMAT);
        this.error = error;
        this.data = (ex == null ? null : ex.getMessage());
    }

    public ResponseEntity(String requestId, Date at, Integer code, String message) {
        this.requestId = requestId;
        this.at = DateFormatUtils.format(at, Constant.DATETIME_FORMAT);
        this.error = new ResponseError(code, message);
    }

    public ResponseEntity(String requestId, Date at, Integer code, String message, Exception ex) {
        this.requestId = requestId;
        this.at = DateFormatUtils.format(at, Constant.DATETIME_FORMAT);
        this.error = new ResponseError(code, message);
        this.data = (ex == null ? null : ex.getMessage());
    }

    public ResponseEntity(String requestId, Date at, Integer code, String message, Object data) {
        this.requestId = requestId;
        this.at = DateFormatUtils.format(at, Constant.DATETIME_FORMAT);
        this.error = new ResponseError(code, message);
        this.data = data;
    }

    public ResponseEntity(String requestId, Date at, Integer code, String message, Object data, Exception ex) {
        this.requestId = requestId;
        this.at = DateFormatUtils.format(at, Constant.DATETIME_FORMAT);
        this.error = new ResponseError(code, message);
        this.data = data;
    }


    @Override
    public String toString() {
        String strResponse = new Gson().toJson(this);
        return strResponse.length() < 1000 ?
                strResponse :
                strResponse.substring(0, 1000) + "...";
    }

}
