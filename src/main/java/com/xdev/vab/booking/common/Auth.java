package com.xdev.vab.booking.common;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class Auth {
    private Integer id;
    private String userName;
    private List<String> roles;
}
