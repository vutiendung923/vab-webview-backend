package com.xdev.vab.booking.common;

public interface Roles {
    String ARTIST = "ARTIST";
    String CUSTOMER = "CUSTOMER";
    String VIEWER = "VIEWER";
}
