package com.xdev.vab.booking.exception;

import javax.ws.rs.NotFoundException;

public class RoleNotFoundException extends NotFoundException {
    public RoleNotFoundException(String role) {
        super(String.format("Không tìm thấy quyền `%s` !", role));
    }
}
