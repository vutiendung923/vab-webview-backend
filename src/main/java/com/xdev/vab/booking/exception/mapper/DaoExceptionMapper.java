package com.xdev.vab.booking.exception.mapper;

import com.xdev.vab.booking.common.Constant;
import com.xdev.vab.booking.common.ResponseEntity;
import com.xdev.vab.booking.exception.DaoException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.util.Date;

public class DaoExceptionMapper implements ExceptionMapper<DaoException> {
    @Context
    private HttpServletRequest httpServletRequest;

    private final Log log = LogFactory.getLog(this.getClass());

    @Override
    public Response toResponse(DaoException e) {
        String requestId = (String) httpServletRequest.getAttribute(Constant.TRACING_LOG_ATTRIBUTE_NAME);
        log.error(String.format("requestId: %s, [DAO EXCEPTION] ==> %s", requestId, e.getMessage()), e);
        return Response.ok(new ResponseEntity(requestId, new Date(), e.getCode(), e.getMessage())).build();
    }
}
