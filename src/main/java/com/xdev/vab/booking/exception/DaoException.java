package com.xdev.vab.booking.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class DaoException extends RuntimeException {
    private String requestId;
    private Integer code;

    public DaoException(String requestId, Integer code, String message) {
        super(message);
        this.requestId = requestId;
        this.code = code;
    }
}
