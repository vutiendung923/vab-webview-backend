package com.xdev.vab.booking.utils;

import lombok.SneakyThrows;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class CryptoUtils {
    @SneakyThrows
    public static String encodeMD5(String message) {
        MessageDigest msgDigest = MessageDigest.getInstance("MD5");
        return (new BigInteger(1, msgDigest.digest(message.getBytes(StandardCharsets.UTF_8)))).toString(16);
    }

    public static void main(String[] args) {
        System.err.println(encodeMD5("123@123a"));
    }
}
