package com.xdev.vab.booking.utils;

import com.xdev.vab.booking.sys.model.Artist;
import com.xdev.vab.booking.sys.model.ArtistBooking;
import lombok.SneakyThrows;
import org.apache.commons.lang3.time.DateUtils;

import javax.persistence.criteria.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

public class QueryUtils {
    public static String buildLikeExp(final String query) {
        if (query == null || !H.isTrue(query.trim())) {
            return null;
        }
        return "%" + query.trim().replaceAll("\\s+", "%") + "%";
    }

    public static <T> Predicate buildLikeFilter(final Root<T> root, final CriteriaBuilder cb, final String keyword, final String... fieldNames) {
        final String likeExp = buildLikeExp(keyword);
        if (!H.isTrue(likeExp) || !H.isTrue(fieldNames)) {
            return cb.and();
        }
        return cb.or(H.collect(Arrays.asList(fieldNames), (index, fieldName) -> cb.like(cb.upper(root.get(fieldName)), likeExp.toUpperCase())).toArray(new Predicate[0]));
    }

    public static <T> Predicate buildSimpleLikeFilter(final Root<T> root, final CriteriaBuilder cb, final String keyword, final String... fieldNames) {
        if (!H.isTrue(keyword) || !H.isTrue(fieldNames)) {
            return cb.and();
        }
        return cb.or(H.collect(Arrays.asList(fieldNames), (index, fieldName) -> cb.like(cb.upper(root.get(fieldName)), ("%" + keyword + "%").toUpperCase())).toArray(new Predicate[0]));
    }

    public static <T, P> Predicate buildEqFilter(final Root<T> root, final CriteriaBuilder cb, final String fieldName, final P value) {
        return H.isTrue(value) ? cb.equal(root.get(fieldName), value) : cb.and();
    }

    public static <T, P> Predicate buildEqFilter(final Root<T> root, final CriteriaBuilder cb, final Path fieldName, final P value) {
        return H.isTrue(value) ? cb.equal(fieldName, value) : cb.and();
    }



    public static <T> Predicate buildIsDeleteFilter(final Root<T> root, final CriteriaBuilder cb) {
        return cb.equal(root.get("isDelete"), false);
    }

    public static <T, P> Predicate buildInFilter(final Root<T> root, final CriteriaBuilder cb, final String fieldName, final Collection<P> values) {
        return H.isTrue(values) ? root.get(fieldName).in(values) : cb.and();
    }

    @SneakyThrows
    public static <T, P> Predicate buildGreaterThanFilter(final Root<T> root, final CriteriaBuilder cb,
                                                          final String fieldName, String valueStart, String pattern) {

        return H.isTrue(valueStart) ? cb.greaterThan(cb.function("TRUNC", Date.class, root.get(fieldName)),
                DateUtils.parseDate(valueStart, pattern)) : cb.and();
    }

    @SneakyThrows
    public static <T, P> Predicate buildLessThanFilter(final Root<T> root, final CriteriaBuilder cb,
                                                       final String fieldName, String valueEnd, String pattern) {

        return H.isTrue(valueEnd) ? cb.lessThan(cb.function("TRUNC", Date.class, root.get(fieldName)),
                DateUtils.parseDate(valueEnd, pattern)) : cb.and();
    }

    public static <T> Predicate buildLikeFilter(final Root<T> root, final CriteriaBuilder cb, final String keyword, final Path... paths) {
        final String likeExp = buildLikeExp(keyword);
        if (!H.isTrue(likeExp)) {
            return cb.and();
        }
        return cb.or(H.collect(Arrays.asList(paths), (index, path) -> cb.like(cb.upper((Expression<String>) path), likeExp.toUpperCase())).toArray(new Predicate[0]));
    }


}
